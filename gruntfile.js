module.exports = function (grunt) {
    grunt.initConfig({


    imagemin: {                          // Task
         
        dynamic: {                         // Another target
          files: [{
            expand: true,                  // Enable dynamic expansion
            cwd: 'wp-content/uploads/',                   // Src matches are relative to this path
            src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
            dest: 'wp-content/uploads/'                  // Destination path prefix
          }]
        }
    },

    // define source files and their destinations
    uglify: {
        min: {
            files: grunt.file.expandMapping([
                'wp-content/themes/cleansale/scripts/**/*.js', 
                '!wp-content/themes/cleansale/scripts/**/*.min.js',

                'wp-content/plugins/**/**/*.js',
                '!wp-content/plugins/**/**/*.min.js',

                'wp-content/themes/cleansale/ocmx/**/*.js',
                '!node_modules/*.js'], 'assets/', {
                rename: function(destBase, destPath) {
                    return destPath.replace('.js', '.js');
                }
            })
        }
    }

});
 
// load plugins
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-imagemin');

// register at least this one task
grunt.registerTask('js', [ 'uglify' ]);
grunt.registerTask('images', [ 'imagemin' ]);
grunt.registerTask('default', [ 'uglify', 'imagemin' ]);


};

