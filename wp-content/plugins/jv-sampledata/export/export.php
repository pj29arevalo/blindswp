<?php
if (!function_exists ('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}
class jvthemeExport {

	function init_jvtheme_export() {

		$go = isset( $_GET[ 'aexport' ] ) && ( $name = $_GET[ 'aexport' ] );

		if( !$go ) { return false; }

		$methods = array( 
			'content' 		=> 'export_content'
			,'revslider'	=> 'export_revslider'
			,'customizer' 	=> 'export_customizer_options' 
			,'menu' 		=> 'export_jvtheme_menus' 
			,'page' 		=> 'export_page_options' 
			,'widget' 		=> 'export_widgets_sidebars'
		);

		$go 	= isset( $methods[ $name ] ) && method_exists( $this, $methods[ $name ] );

		$name 	= $methods[ $name ];

		if( !$go ) { return false; }

		$this->{$name}();
	}

	public function export_content() {

		ob_start();
		$q = array(
			'download'			=>'true'
			,'content'			=>'all'
			,'cat'				=>'0'
			,'post_author'		=>'0'
			,'post_start_date'	=>'0'
			,'post_end_date'	=>'0'
			,'post_status'		=>'0'
			,'page_author'		=>'0'
			,'page_start_date'	=>'0'
			,'page_end_date'	=>'0'
			,'page_status'		=>'0'
			,'submit'			=>'Download Export File'
		);
		
		if( !function_exists( 'export_wp' ) ) {
			
			require_once( __DIR__ . '/wp.export.php' );

		}

		$args = array( 
           'content'    => 'all',
           'author'     => false,
           'category'   => false,
           'start_date' => false,
           'end_date'   => false,
           'status'     => false,
	   	);
		$args = apply_filters( 'export_args', $args );

		export_wp( $args );

		$output = ob_get_clean();
		
		$this->save_as_txt_file( "content.xml", $output );
	}

	public function export_customizer_options(){
		$mods = get_theme_mods();
		unset( $mods['nav_menu_locations'] );

		$output = base64_encode(serialize($mods));
		$this->save_as_txt_file("customizer_options.txt", $output);
	}
	public function export_options(){
		$jvtheme_options = get_option("jvtheme_options_proya");
		$output = base64_encode(serialize($jvtheme_options));
		$this->save_as_txt_file("options.txt", $output);
	}
	public function export_widgets_sidebars(){
		$this->data = array();
		$this->data['sidebars'] = $this->export_sidebars();
		$this->data['widgets'] 	= $this->export_widgets();

		$output = base64_encode(serialize($this->data));
		$this->save_as_txt_file("widgets.txt", $output);
	}
	public function export_widgets(){

		global $wp_registered_widgets;
		$all_jvtheme_widgets = array();

		foreach ($wp_registered_widgets as $jvtheme_widget_id => $widget_params)
			$all_jvtheme_widgets[] = $widget_params['callback'][0]->id_base;

		foreach ($all_jvtheme_widgets as $jvtheme_widget_id) {
			$jvtheme_widget_data = get_option( 'widget_' . $jvtheme_widget_id );
			if ( !empty($jvtheme_widget_data) )
				$widget_datas[ $jvtheme_widget_id ] = $jvtheme_widget_data;
		}
		unset($all_jvtheme_widgets);
		return $widget_datas;

	}
	public function export_sidebars(){
		$jvtheme_sidebars = get_option("sidebars_widgets");
		$jvtheme_sidebars = $this->exclude_sidebar_keys($jvtheme_sidebars);
		return $jvtheme_sidebars;
	}
	private function exclude_sidebar_keys( $keys = array() ){
		if ( ! is_array($keys) )
			return $keys;

		unset($keys['wp_inactive_widgets']);
		unset($keys['array_version']);
		return $keys;
	}
	public function export_jvtheme_menus(){
		global $wpdb;

		$this->data = array();
		$locations = get_nav_menu_locations();

		$terms_table = $wpdb->prefix . "terms";
		foreach ((array)$locations as $location => $menu_id) {
			$menu_slug = $wpdb->get_results("SELECT * FROM $terms_table where term_id={$menu_id}", ARRAY_A);
			if (!empty($menu_slug))
			{
				$this->data[ $location ] = $menu_slug[0]['slug'];
			}
		}

		$output = base64_encode(serialize( $this->data ));
		$this->save_as_txt_file("menus.txt", $output);
	}
	public function export_page_options(){
        
        if( !class_exists( 'JVLibrary' ) ) {
            require_once( ABSPATH . '/wp-content/themes/jv-allinone/library/jvLibrary.php' );   
        }
        
		$jvtheme_static_page = get_option("page_on_front");
		$jvtheme_post_page = get_option("page_for_posts");
		$jvtheme_show_on_front = get_option("show_on_front");
		$jvtheme_settings_pages = array(
			'show_on_front' => $jvtheme_show_on_front,
			'page_on_front' => $jvtheme_static_page,
			'page_for_posts' => $jvtheme_post_page,
            'megamenu_settings' => get_option( 'megamenu_settings' ),
            'megamenu_themes' => get_option( 'megamenu_themes' ),
            'woocommerce_shop_page_id' => get_option( 'woocommerce_shop_page_id' ),
            'woocommerce_cart_page_id' => get_option( 'woocommerce_cart_page_id' ),
            'woocommerce_checkout_page_id' => get_option( 'woocommerce_checkout_page_id' ),
            'woocommerce_myaccount_page_id' => get_option( 'woocommerce_myaccount_page_id' ),
            'yith_wcwl_wishlist_page_id' => get_option( 'yith_wcwl_wishlist_page_id' ),
            'yith-wcwl-page-id' => get_option( 'yith-wcwl-page-id' ),
            'yith_wcwl_button_position' => get_option( 'yith_wcwl_button_position' ),
            'yith_woocompare_compare_button_in_product_page' => get_option( 'yith_woocompare_compare_button_in_product_page' ),
            'bp-active-components' => get_option( 'bp-active-components' ),
            'bp-pages' => get_option( 'bp-pages' ),
            '_theme_settings' => JVLibrary::getConfig(false)
		);
		$output = base64_encode(serialize($jvtheme_settings_pages));
		$this->save_as_txt_file("page_options.txt", $output);
	}
	public function export_essential_grid() {
	
		$lib = ABSPATH .'wp-content/plugins/essential-grid/essential-grid.php';
		
		if( !file_exists( $lib ) ) { return false; }
		require_once( $lib );

		$c_grid = new Essential_Grid();

		$export_grids = array();
		$grids = $c_grid->get_essential_grids();
		foreach ($grids as $grid) {
			$export_grids[] = $grid->id;
		}

		$export_skins = array();
		$item_skin = new Essential_Grid_Item_Skin();
		$skins = $item_skin->get_essential_item_skins('all', false);
		foreach ($skins as $skin) {
			$export_grids[] = $skin['id'];
		}

		$export_elements = array();
		$c_elements = new Essential_Grid_Item_Element();
		$elements = $c_elements->get_essential_item_elements();
		foreach ($elements as $element) {
			$export_elements[] = $element['id'];
		}

		$export_navigation_skins = array();
		$c_nav_skins = new Essential_Grid_Navigation();
		$nav_skins = $c_nav_skins->get_essential_navigation_skins();
		foreach ($nav_skins as $nav_skin) {
			$export_navigation_skins[] = $nav_skin['id'];
		}


		$export_custom_meta = array();
		$metas = new Essential_Grid_Meta();
		$custom_metas = $metas->get_all_meta();
		foreach ($custom_metas as $custom_meta) {
			$export_custom_meta[] = $custom_meta['handle'];
		}

		$export_punch_fonts = array();
		$fonts = new ThemePunch_Fonts();
		$custom_fonts = $fonts->get_all_fonts();
		foreach ($custom_fonts as $custom_font) {
			$export_punch_fonts[] = $custom_font['handle'];
		}

		$export = array();

		$ex = new Essential_Grid_Export();

		//export Grids
		if(!empty($export_grids))
			$export['grids'] = $ex->export_grids($export_grids);

		//export Skins
		if(!empty($export_skins))
			$export['skins'] = $ex->export_skins($export_skins);

		//export Elements
		if(!empty($export_elements))
			$export['elements'] = $ex->export_elements($export_elements);

		//export Navigation Skins
		if(!empty($export_navigation_skins))
			$export['navigation-skins'] = $ex->export_navigation_skins($export_navigation_skins);

		//export Custom Meta
		if(!empty($export_custom_meta))
			$export['custom-meta'] = $ex->export_custom_meta($export_custom_meta);

		//export Punch Fonts
		if(!empty($export_punch_fonts))
			$export['punch-fonts'] = $ex->export_punch_fonts($export_punch_fonts);

		//export Global Styles
		$export['global-css'] = $ex->export_global_styles('on');

		$this->save_as_txt_file('essential_grid.txt', json_encode($export));
	}

	function export_revslider() {
		
		if( !class_exists( 'RevSlider' ) ) { return false; }

		if( !class_exists( 'JVRevSlider' ) ) {
			require_once( __DIR__ . '/revslider_sider.class.ex.php' );
		}

		$slider = new JVRevSlider();
		
		foreach( $slider->getAllSlider() as $item ) {

			$slider->initByDBData( $item );

			$slider->exportSlider();
		}
	}

	function save_as_txt_file($file_name, $output){
		
		$path = apply_filters( 'jvtheme_import_demos', 'theme01' );
        
        do_action( 'jvtheme_cfolder', $path );

		$path = jvdatasample::getImportDataFolderPath() . $path;

		$path = "{$path}/{$file_name}";
		
		file_put_contents( $path, $output );
	}

}

if( !function_exists( 'jvtheme_export' ) ) {

	function jvtheme_export() {
		
		$my_jvtheme_Export = new jvthemeExport();
		
		$my_jvtheme_Export->init_jvtheme_export();

	}

	add_action( 'wp_ajax_jvtheme_export', 'jvtheme_export' );
}