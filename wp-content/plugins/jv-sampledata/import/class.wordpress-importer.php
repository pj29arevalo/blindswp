<?php

if ( ! defined( 'WP_LOAD_IMPORTERS' ) )
	//return;

/** Display verbose errors */
define( 'IMPORT_DEBUG', false );

// Load Importer API
require_once ABSPATH . 'wp-admin/includes/import.php';

if ( ! class_exists( 'WP_Importer' ) ) {
	$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
	if ( file_exists( $class_wp_importer ) )
		require $class_wp_importer;
}

if ( ! class_exists( 'WXR_Parser' ) ) {
require_once dirname( __FILE__ ) . '/parsers.php';
}

/**
 * WordPress Importer class for managing the import process of a WXR file
 *
 * @package WordPress
 * @subpackage Importer
 */
class WP_Import extends WP_Importer {
	var $max_wxr_version = 1.2; // max. supported WXR version
	
	var $id; // WXR attachment ID

	// information to import from WXR file
	var $version;
	var $authors           = array();
	var $posts             = array();
	var $terms             = array();
	var $categories        = array();
	var $tags              = array();
	var $base_url          = '';

	// mappings from old information to new
	var $cache_terms       = array();
	var $fetch_attachments = true; 
    
    // CONST QUERY
    var $QUERY_TERM = "REPLACE INTO `%s` (`term_id`, `name`, `slug`, `term_group`) VALUES ('%s', '%s', '%s', '%s')";
    var $QUERY_TERM_TAXONOMY = "REPLACE INTO `%s` (`term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES ('%s', '%s', '%s', '%s', '%s');";       
    var $QUERY_TERM_TAXONOMY_UPDATE = "UPDATE `%s` SET `count`= (`count` + 1) WHERE (`term_taxonomy_id`='%s');"; 
    var $QUERY_TERM_RELATIONSHIPS = "REPLACE INTO `%s` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES ('%s', '%s', '0');";
    var $QUERY_POST = "REPLACE INTO `%s` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `post_parent`, `guid`, `menu_order`, `post_type`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');";
    var $QUERY_POST_UPDATE = "UPDATE `%s` SET post_mime_type='%s' WHERE ID='%s';";
    var $QUERY_POST_META = "REPLACE INTO `%s` (`post_id`, `meta_key`, `meta_value`) VALUES ('%s', '%s', '%s');"; 
    
    
    // fields term
    var $fields = array(
        'cat' => array(
            'term_id'   => 'term_id',
            'slug'      => 'category_nicename',
            'parent'    => 'category_parent',
            'name'      => 'cat_name',
            'desc'      => 'category_description',
        ),
        'tag' => array(
            'term_id'   => 'term_id',
            'slug'      => 'tag_slug',
            'parent'    => 0,
            'name'      => 'tag_name',
            'desc'      => 'tag_description',
        ),
        'term' => array(
            'term_id'   => 'term_id',
            'taxonomy'  => 'term_taxonomy',
            'slug'      => 'slug',
            'parent'    => 'term_parent',
            'name'      => 'term_name',
            'desc'      => 'term_description',
        ),
        'post' => array(
            'post_id'           => 'ID',
            'post_author'       => 'post_author',
            'post_date'         => 'post_date',
            'post_date_gmt'     => 'post_date_gmt',
            'post_content'      => 'post_content',
            'post_title'        => 'post_title',
            'post_excerpt'      => 'post_excerpt',
            'status'            => 'post_status',
            'comment_status'    => 'comment_status',
            'ping_status'       => 'ping_status',
            'post_password'     => 'post_password',
            'post_name'         => 'post_name',                  
            'post_parent'       => 'post_parent',
            'guid'              => 'guid',
            'menu_order'        => 'menu_order',
            'post_type'         => 'post_type', 
        )
    );

	function WP_Import() { /* nothing */ }


	/**
	 * The main controller for the actual import stage.
	 *
	 * @param string $file Path to the WXR file for importing
	 */
	function import( $file ) {
        
        add_filter( 'http_request_timeout', array( &$this, 'bump_request_timeout' ) );
		
		$this->import_start( $file );

		wp_suspend_cache_invalidation( true );
		$this->import_zterms();
        $this->import_zposts();  
		wp_suspend_cache_invalidation( false );

		$this->import_end();
	}
    
    /**
    * get field term
    */
     function get_term_field( $item = array() ) {    
         
         if( array_key_exists( 'cat_name', $item ) ) { return 'cat'; }
         
         if( array_key_exists( 'tag_name', $item ) ) { return 'tag'; }
         
         if( array_key_exists( 'term_name', $item ) ) { return 'term'; }
         
         return 0;
     }
     
     /**
    * get taxonomy term
    */
     function get_term_taxonomy( $t = '' ) {
         
         $rs = '';
         
         switch( $t ) {
             
             case 'cat': $rs = 'category'; break;
             
             case 'tag': $rs = 'post_tag'; break;   
             
         }
         
         return $rs;
     }
    
    /**
     * get parent of term
     * 
     */
     function get_term_id( $slug = '' ) {
         
         if( !$this->cache_terms ) { return 0; }
         
         $rs = 0;
         
         foreach( $this->cache_terms as $item ) {
             
             if( isset( $item[ 'term_parent' ] ) && !strcmp( $slug, $item[ 'slug' ] ) ) {
                 
                 $rs = $item[ 'term_id' ];
                 
                 break;
             }
             
             if( isset( $item[ 'category_parent' ] ) && !strcmp( $slug, $item[ 'category_nicename' ] ) ) {
                 
                 $rs = $item[ 'term_id' ];
                 
                 break;
             }
         }
         
         return $rs;
     } 
     
     /**
     * import zterm
     */
     function import_zterms() {
        
        $d = apply_filters( 'wp_import_zterms', $this->cache_terms );

        if ( empty( $d ) )
            return;
            
        global $wpdb;       
        $tbl_terms          = $wpdb->terms;
        $tbl_term_taxonomy  = $wpdb->term_taxonomy;
        
        foreach( array( $tbl_term_taxonomy ) as $tbl ) {
            
            $wpdb->query( "TRUNCATE {$tbl}" ); 
                
        }                                                  

        foreach ( $d as $item ) {
            
            $field_type = $this->get_term_field( $item );
            
            if( !$field_type ) { continue; }
            
            $field = $this->fields[ $field_type ];
            
            $term_id   = $item[ $field[ 'term_id' ] ];
            $q         = sprintf( $this->QUERY_TERM, 
                $tbl_terms, 
                $term_id, 
                @mysql_escape_string( $item[ $field[ 'name' ] ] ), 
                $item[ $field[ 'slug' ] ], 
                0
            );
            
            $wpdb->query( $q );                  
            
            $parent = !$field[ 'parent' ] ? 0 : $item[ $field[ 'parent' ] ];
            $desc   = isset( $item[ $field[ 'desc' ] ] ) ? $item[ $field[ 'desc' ] ] : ''; 
            
            if( $parent ) { $parent = $this->get_term_id( $parent ); }
            
            $taxonomy = '';
            
            if( isset( $field[ 'taxonomy' ] ) ) {
                
                $taxonomy = isset( $item[ $field[ 'taxonomy' ] ] ) ? $item[ $field[ 'taxonomy' ] ] : '';
                    
            }else {
                
                $taxonomy = $this->get_term_taxonomy( $field_type );
            }       
            
            // term taxonomy
            if( !$taxonomy ) { continue; }
            
            $q      = sprintf( $this->QUERY_TERM_TAXONOMY, 
                $tbl_term_taxonomy
                ,$term_id
                ,$taxonomy
                ,@mysql_escape_string( $desc )
                ,$parent
                ,0
            );
            
            $wpdb->query( $q );
            
        }
            
     }
     
     /**
     * filter data post
     */
     function post_data_draw( $post = array() ) {
         
         $rs = array();
         
         foreach( $this->fields[ 'post' ] as $fk => $fv ) {
             
             if( !isset( $post[ $fk ] ) ) { continue; }
             
             $rs[ $fv ] = $post[ $fk ];
         }
         
         return $rs;
     }
     
     /**
     * do post terms
     */
     function do_post_terms( $post = array() ) {
         
         if( !isset( $post[ 'terms' ] ) || !is_array( $post[ 'terms' ]) ) { return false;}
         
         global $wpdb; 
         $tbl_relate    = $wpdb->term_relationships;
         $tbl_taxonomy  = $wpdb->term_taxonomy;
         
         foreach( $post[ 'terms' ] as $item ) {
             
             $term = term_exists( $item[ 'slug' ], $item[ 'domain' ] );
             
             if( !$term ) {
                
                 $term_arr  = array( 'description' => '', 'slug' => $item[ 'slug' ], 'parent' => 0 );
                 
                 $term_id   = wp_insert_term( $item[ 'name' ], $item[ 'domain'], $term_arr );
                 
                 if( is_wp_error( $term_id ) ) { continue; }
                 
                 $term = term_exists( $item[ 'slug' ], $item[ 'domain' ] );
             }
             
             $q = sprintf( 
                $this->QUERY_TERM_RELATIONSHIPS,
                $tbl_relate,
                $post[ 'post_id' ],
                $term[ 'term_taxonomy_id' ]
             );
             
             $wpdb->query( $q );
             
             $q = sprintf( 
                $this->QUERY_TERM_TAXONOMY_UPDATE,
                $tbl_taxonomy,
                $term[ 'term_taxonomy_id' ]
             );
             
             $wpdb->query( $q );
             
         }
     }
     
     /**
     * do post meta
     */
     function do_post_meta( $post = array() ) {
         
         if( !isset( $post[ 'postmeta' ] ) || !is_array( $post[ 'postmeta' ] ) ) { continue; }
         
         global $wpdb;
         $tbl_name = $wpdb->postmeta;
         
         foreach( $post[ 'postmeta' ] as $item ) {
             
             $q = sprintf(
                $this->QUERY_POST_META,
                $tbl_name,
                $post[ 'post_id' ],
                $item[ 'key' ],
                @mysql_escape_string( $item[ 'value' ] )
             );
             
            $wpdb->query( $q );
         }    
     }
     
     /**
     * import posts
     * 
     */
     function import_zposts() {
        
        $posts      = apply_filters( 'wp_import_posts', $this->posts ); 
        $author_id  = get_current_user_id();
        global $wpdb;
        $tbl_posts = $wpdb->posts;
        
        foreach( array( $wpdb->posts, $wpdb->term_relationships, $wpdb->postmeta ) as $tbl ){ 
            
            $wpdb->query( "TRUNCATE {$tbl}" );
            
        }

        foreach ( $posts as $post ) {
            
            if ( isset( $post['status'] ) && $post['status'] == 'auto-draft' ) { continue; }
                
                
            $post_data = $this->post_data_draw( $post );
            $post_data = apply_filters( 'wp_import_post_data_raw', $post_data );
            
            $q = sprintf( $this->QUERY_POST, 
                $tbl_posts,
                $post_data[ 'ID' ],
                $author_id,
                $post_data[ 'post_date' ],
                $post_data[ 'post_date_gmt' ],
                @mysql_escape_string( $post_data[ 'post_content' ] ),
                $post_data[ 'post_title' ],
                $post_data[ 'post_excerpt' ],
                $post_data[ 'post_status' ],
                $post_data[ 'comment_status' ],
                $post_data[ 'ping_status' ],
                $post_data[ 'post_password' ],
                @mysql_escape_string( $post_data[ 'post_name' ] ),                  
                $post_data[ 'post_parent' ],
                $post_data[ 'guid' ],
                $post_data[ 'menu_order' ],
                $post_data[ 'post_type' ]   
            );
            
            $wpdb->query( $q );
            
            if( isset( $post[ 'terms' ] ) ){ $this->do_post_terms( $post ); }
            
            if ( isset( $post['postmeta'] ) ) { $this->do_post_meta( $post ); }
            
            if ( 'attachment' == $post_data['post_type'] ) { 

                $this->do_attachment( $post );

            }
            
            if ( $post['is_sticky'] == 1 ) { stick_post( $post[ 'post_id' ] ); }
            
        }    
     }

	/**
	 * Parses the WXR file and prepares us for the task of processing parsed data
	 *
	 * @param string $file Path to the WXR file for importing
	 */
	function import_start( $file ) {
		if ( ! is_file($file) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.', 'wordpress-importer' ) . '</strong><br />';
			echo __( 'The file does not exist, please try again.', 'wordpress-importer' ) . '</p>';     
		}

		$import_data = $this->parse( $file );

		if ( is_wp_error( $import_data ) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.', 'wordpress-importer' ) . '</strong><br />';
			echo esc_html( $import_data->get_error_message() ) . '</p>';
		}

		$this->version = $import_data['version'];
		$this->posts = $import_data['posts'];
		$this->terms = $import_data['terms'];
		$this->categories = $import_data['categories'];
		$this->tags = $import_data['tags'];
        $this->cache_terms = array_merge( $this->categories, $this->tags, $this->terms );
		$this->base_url = esc_url( $import_data['base_url'] );

		wp_defer_term_counting( true );
		wp_defer_comment_counting( true );

		do_action( 'import_start' );
	} 

	/**
	 * Performs post-import cleanup of files and the cache
	 */
	function import_end() {
		wp_import_cleanup( $this->id );

		wp_cache_flush();
		foreach ( get_taxonomies() as $tax ) {
			delete_option( "{$tax}_children" );
			_get_term_hierarchy( $tax );
		}

		wp_defer_term_counting( false );
		wp_defer_comment_counting( false );

		do_action( 'import_end' );
	}

    /**
     * do attachment
     */
     function do_attachment( $post = array() ) {
         
        
        if ( !isset( $post[ 'postmeta' ] ) ) { return false; }
                                                           
        foreach( $post[ 'postmeta' ] as $meta ) {
                
            if( $meta[ 'key' ] !== '_wp_attachment_metadata' ) { continue; }
            
            $value = maybe_unserialize( $meta[ 'value' ] );


            if ( isset( $value[ 'file' ] ) && ( $info = wp_check_filetype( $value[ 'file' ] ) ) ) {

                global $wpdb;    
                
                $q = sprintf( $this->QUERY_POST_UPDATE, $wpdb->posts, $info['type'], $post[ 'post_id' ] );

                $wpdb->query( $q );
            }
            
            $this->process_attachment( $value );
            
            $sub = isset( $value[ 'file' ] ) ? pathinfo( $value[ 'file' ] ) : '';
            if( is_array( $sub ) ) { $sub = $sub[ 'dirname' ]; }
            
            if( !isset( $value[ 'sizes' ] ) ) { continue; }
            
            foreach( $value[ 'sizes' ] as $item ){ 
                
                $this->process_attachment( $item, $sub );
                
            }
        }

     }
     
     function process_attachment( $data = array(), $sub = '' ) {
         
        if( !isset( $data[ 'file' ] ) ) { return false; }
         
        $dir     = WP_CONTENT_DIR . '/uploads' . ( $sub ? "/{$sub}" : '' );
        $dest_file = "{$dir}/{$data[ 'file' ]}";
        
        
        if( file_exists( $dest_file ) ) { return false; }                                                

        $pathinfo = pathinfo( $dest_file );
        
        apply_filters( 'jvtheme_cfolder', $pathinfo[ 'dirname' ], true );
         
        $this->draw( $dest_file, "{$data['width']}x{$data['height']}");
        
     }

     function hex2rgb($colour) {
        $colour = preg_replace("/[^abcdef0-9]/i", "", $colour);
        if (strlen($colour) == 6)
        {
            list($r, $g, $b) = str_split($colour, 2);
            return Array("r" => hexdec($r), "g" => hexdec($g), "b" => hexdec($b));
        }
        elseif (strlen($colour) == 3)
        {
            list($r, $g, $b) = array($colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2]);
            return Array("r" => hexdec($r), "g" => hexdec($g), "b" => hexdec($b));    
        }
        return false;
    }

    function draw( $dest_file = '', $size = '100x100', $bg = 'ccc', $fg = '555', $text = '' ) {
        // Dimensions
        $getsize    = $size ? $size : '100x100';
        $dimensions = explode('x', $getsize);

        // Create image
        $image      = imagecreate($dimensions[0], $dimensions[1]);

        // Colours
        $bg         = $bg ? $bg : 'ccc';
        $bg         = $this->hex2rgb($bg);
        $setbg      = imagecolorallocate($image, $bg['r'], $bg['g'], $bg['b']);

        $fg         = $fg ? $fg : '555';
        $fg         = $this->hex2rgb($fg); 
        $setfg      = imagecolorallocate($image, $fg['r'], $fg['g'], $fg['b']);

        // Text
        $text       = $text ? strip_tags($text) : $getsize;
        $text       = str_replace('+', ' ', $text);

        // Text positioning
        $fontsize   = 4;
        $fontwidth  = imagefontwidth($fontsize);    // width of a character
        $fontheight = imagefontheight($fontsize);   // height of a character
        $length     = strlen($text);                // number of characters
        $textwidth  = $length * $fontwidth;         // text width
        $xpos       = (imagesx($image) - $textwidth) / 2;
        $ypos       = (imagesy($image) - $fontheight) / 2;

        // Generate text
        imagestring($image, $fontsize, $xpos, $ypos, $text, $setfg);

        // Render image
        imagepng($image, $dest_file );

        // Restroy
        imagedestroy( $image );
    }

	/**
	 * Parse a WXR file
	 *
	 * @param string $file Path to WXR file for parsing
	 * @return array Information gathered from the WXR file
	 */
	function parse( $file ) {
		if ( ! class_exists( 'WXR_Parser' ) ) {
			require_once dirname( __FILE__ ) . '/parsers.php';
		}
		$parser = new WXR_Parser();
		return $parser->parse( $file );
	}

	
	/**
	 * Decide if the given meta key maps to information we will want to import
	 *
	 * @param string $key The meta key to check
	 * @return string|bool The key if we do want to import, false if not
	 */
	function is_valid_meta_key( $key ) {
		// skip attachment metadata since we'll regenerate it from scratch
		// skip _edit_lock as not relevant for import
		if ( in_array( $key, array( '_wp_attached_file', '_wp_attachment_metadata', '_edit_lock' ) ) )
			return false;
		return $key;
	}

	/**
	 * Decide whether or not the importer is allowed to create users.
	 * Default is true, can be filtered via import_allow_create_users
	 *
	 * @return bool True if creating users is allowed
	 */
	function allow_create_users() {
		return apply_filters( 'import_allow_create_users', true );
	}

	/**
	 * Decide whether or not the importer should attempt to download attachment files.
	 * Default is true, can be filtered via import_allow_fetch_attachments. The choice
	 * made at the import options screen must also be true, false here hides that checkbox.
	 *
	 * @return bool True if downloading attachments is allowed
	 */
	function allow_fetch_attachments() {
		return apply_filters( 'import_allow_fetch_attachments', true );
	}

	/**
	 * Decide what the maximum file size for downloaded attachments is.
	 * Default is 0 (unlimited), can be filtered via import_attachment_size_limit
	 *
	 * @return int Maximum attachment file size to import
	 */
	function max_attachment_size() {
		return apply_filters( 'import_attachment_size_limit', 0 );
	}

	/**
	 * Added to http_request_timeout filter to force timeout at 60 seconds during import
	 * @return int 60
	 */
	function bump_request_timeout($val) {
		return 600;
	}

	// return the difference in length between two strings
	function cmpr_strlen( $a, $b ) {
		return strlen($b) - strlen($a);
	}
}



