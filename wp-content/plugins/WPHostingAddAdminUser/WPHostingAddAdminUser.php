<?php
/**
 *
 * @package   WP Hosting Add Admin User
 * @author    support@wphosting.com.au
 * @license   GPL-2.0+
 * @link      http://wphosting.com.au
 * @copyright 2014 WP Hosting 
 *
 * @wordpress-plugin
 * Plugin Name:       WP Hosting Add Admin User 
 * Plugin URI:        http://wphosting.com.au
 * Description:       Adds a Username and Password For WP Hosting Support, This infomation is sent directly support.
 * Version:           1.1.1
 * Author:            WP Hosting Support Staff
 * Author URI:        https://wphosting.com.au/clients/submitticket.php
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . 'public/class-wphostingaddadminuser.php' );

/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 *
 * @TODO:
 *
 * - replace Plugin_Name with the name of the class defined in
 *   `class-plugin-name.php`
 */
register_activation_hook( __FILE__, array( 'WPHostingAddAdminUser', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'WPHostingAddAdminUser', 'deactivate' ) );