<?PHP

/*-----------------------------------------------------------------------------------*/
/*	Bootstrapn' Time!
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_init(){
	
	// Load Lang
	load_plugin_textdomain( 'wc_tcsd', false, WC_TCSD_RELPATH . '/languages/' );
	
	// HTTPS Check
	global $wc_tcsc_https;
		
	// Make sure we are not in the admin section
	if (!is_admin()) {
				
		// Load jQuery First
		wp_enqueue_script('jquery');
		
		// Only include Facebook if enabled
		if( get_option('wc-tcsd-facebook-enabled') == 'true' || get_option('wc-tcsd-facebook-share-enabled') == 'true' ){
			
			// Setup Locale
			$fb_locale = get_option('wc-tcsd-facebook-api-locale');
			if( $fb_locale == '' ){
				$fb_locale = 'en_US';
			}
			
			// Load Facebook With Custom Locale
			wp_deregister_script('facebook');
			wp_register_script('facebook', $wc_tcsc_https.'//connect.facebook.net/'.$fb_locale.'/all.js#xfbml=1'. false, '1.0', false);
			wp_enqueue_script('facebook');
			
		}
		
		// Only include Twitter if Enabled
		if( get_option('wc-tcsd-twitter-tweet-enabled') == 'true' || get_option('wc-tcsd-twitter-enabled') == 'true' ){
						
			wp_deregister_script('twitter');
			wp_register_script('twitter', $wc_tcsc_https.'//platform.twitter.com/widgets.js'. false, '1.0', false);
			wp_enqueue_script('twitter');
			
		}
		
		// Load TCSD Handler
		wp_deregister_script('tcsd');
		wp_register_script('tcsd', WC_TCSD_LOCATION.'/inc/wctcsd.js', false, WC_TCSD_VERSION, false);
		wp_enqueue_script('tcsd');
				
		// Flush, register, enque Traficc Pop CSS
		wp_deregister_style('tcslCSS');
		wp_register_style('tcslCSS', WC_TCSD_LOCATION.'/css/tcsd.css');
		wp_enqueue_style('tcslCSS');
				
	}
	
	// Make sure our random key is generated
	if( get_option('wc-tcsd-discount-string') == '' ){
		update_option( 'wc-tcsd-discount-string', rand(1000, 999999999) );
	}
	
	// Add TinyMCE
	wc_tcsd_mce();
					
} // End init

/*-----------------------------------------------------------------------------------*/
/*	Add tinyMCE Button
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_mce(){
	
	// Don't bother doing this stuff if the current user lacks permissions
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
	return;
	
	// Add only in Rich Editor mode
	if( get_user_option('rich_editing') == 'true'){
		
		// Add cutom button to TinyMCE
		add_filter('mce_external_plugins', "wc_tcsd_mce_register");
		add_filter('mce_buttons', 'wc_tcsd_add_button', 0);
		
	}
	
}

function wc_tcsd_add_button($buttons) {
   array_push($buttons, "separator", "wctcsd_plugin");
   return $buttons;
}

function wc_tcsd_mce_register($plugin_array) {
   $plugin_array['wctcsd_plugin'] = WC_TCSD_LOCATION."/inc/mce/mce.js";
   return $plugin_array;
}

/*-----------------------------------------------------------------------------------*/
/*	Current Page Function
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_current_page($type = 1){

	if($type == 1){
		
		$pageURL = 'http';
		
		if ($_SERVER["HTTPS"] == "on"){$pageURL .= "s";}
		
		$pageURL .= "://";
		
		if ($_SERVER["SERVER_PORT"] != "80"){
			
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		
		} else {
			
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		
		}
		
		return $pageURL;
		
	} else if($type == 2){
		
		// Get post id and user's IP
		$postID = get_the_ID();
		$postURL = get_permalink($postID);
		return $postURL;
		
	} // end chain

}

/*-----------------------------------------------------------------------------------*/
/*	Checkout Page Handle
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_insert_checkout(){ // Checkout Pages
	
	// Cookie check
	global $post;
	global $woocommerce;
	global $wc_tcsc_options;
	
	// Check if applied
	$applied = 'false';
	if( !empty( $woocommerce->cart->applied_coupons ) ){
        // Loop and see if social coupon is applied
		foreach( $woocommerce->cart->applied_coupons as $code ){
			if( $code == strtolower( $wc_tcsc_options['discount-code'] ) ){
				$applied = 'true';
			}
		}
    }
		
	// Display Message If Applied
	if( $applied == 'true' ){ ?>
	
        <div class="tcsd checkout">
            <div class="tcsd-title"><?PHP _e('Thank You for Sharing!', 'wc_tcsd'); ?></div>
            <div class="tcsd-message"><?PHP _e('Your discount has been activated and applied to your shopping cart.', 'wc_tcsd'); ?></div>
        </div>
        
	<?PHP } else { // otherwise show roll on the buttons
	
		// Only show if enabled on cart pages
		if( get_option('wc-tcsd-checkout') == 'true' ){ echo wc_tcsd_generate($post->ID, 'checkout'); }
		
	} // end if social coupon applied
	
}

/*-----------------------------------------------------------------------------------*/
/*	Product Page Handle
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_insert_product(){ // Product Pages
	
	// Cookie check
	global $post;
	global $wc_tcsc_options;
		
	// Only If Single Discount Enabled
	if( $wc_tcsc_options['discount-type'] == 'multi' ){
		$stringCookie = 'false';
		$postCookie = 'false';
		$usedCookie = 'false';
	} else {
		$thisCookie = "wc_tcsd_".$post->ID;
		$wc_tcsd_discount_string = get_option('wc-tcsd-discount-string');
		if( isset( $_COOKIE[$wc_tcsd_discount_string] ) ){
			$stringCookie = $_COOKIE[$wc_tcsd_discount_string];
		} else {
			$stringCookie = 'false';
		}
		if( isset( $_COOKIE[$thisCookie] ) ){
			$postCookie = $_COOKIE[$thisCookie];
		} else {
			$postCookie = 'false';
		}
		if( isset( $_COOKIE[$wc_tcsd_discount_string.'_used'] ) ){
			$usedCookie = $_COOKIE[$wc_tcsd_discount_string.'_used'];
		} else {
			$usedCookie = 'false';
		}
	}
		
	// Did user already share?
	if( $usedCookie != 'used' ){if($stringCookie == "true" && $postCookie == "true"){
		global $woocommerce;
		$wc_tcsd_discount_code = get_option('wc-tcsd-discount-code');		
		?>
		<div class="tcsd product-page">
            <div class="tcsd-title"><?PHP _e('Thank You for Sharing!', 'wc_tcsd'); ?></div>
            <div class="tcsd-message"><?PHP _e('Your discount has been activated and applied to your shopping cart.', 'wc_tcsd'); ?></div>
		</div>
	<?PHP } else { // otherwise show roll on the buttons
		if( get_option('wc-tcsd-product') == 'true' ){ echo wc_tcsd_generate($post->ID, 'product-page'); }
	}} // end if
	
}

/*-----------------------------------------------------------------------------------*/
/*	Cart Pages Handle
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_insert_cart(){ // Cart Pages
	
	// Cookie check
	global $post;
	global $woocommerce;
	global $wc_tcsc_options;
	
	// Check if applied
	$applied = 'false';
	
	// Only Check for Cookies if Discount Type is single
	if( $wc_tcsc_options['discount-type'] == 'single' ){
		
		if( !empty( $woocommerce->cart->applied_coupons ) ){
			// Loop and see if social coupon is applied
			foreach( $woocommerce->cart->applied_coupons as $code ){
				if( $code == strtolower( $wc_tcsc_options['discount-code'] ) ){
					$applied = 'true';
				}
			}
		}
	
	} // end multi check
	
	// Display Message If Applied
	if( $applied == 'true' ){ ?>
	
        <div class="tcsd cart">
            <div class="tcsd-title"><?PHP _e('Thank You for Sharing!', 'wc_tcsd'); ?></div>
            <div class="tcsd-message"><?PHP _e('Your discount has been activated and applied to your shopping cart.', 'wc_tcsd'); ?></div>
        </div>
        
	<?PHP } else { // otherwise show roll on the buttons
	
		// Only show if enabled on cart pages
		if( get_option('wc-tcsd-cart') == 'true' ){ echo wc_tcsd_generate($post->ID, 'cart'); }
		
	} // end if social coupon applied
	
}

/*-----------------------------------------------------------------------------------*/
/*	Create Checkout / Cart Locker
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_generate($postid, $type){
	
	// Get settings
	global $wc_tcsc_options;
	
	// HTTPS Check
	global $wc_tcsc_https;
	
	// Cached Settings
	$locks = '';
	$theme = 'blue';
	$button_count = 0;
	$thisCookie = "wc_tcsd_".$postid;
	$wc_tcsd_button_layout = 'bubble';
	$wc_tcsd_discount_string = get_option('wc-tcsd-discount-string');
	$wc_tcsd_discount_code = get_option('wc-tcsd-discount-code');
	$wc_tcsd_title = get_option('wc-tcsd-title');
	$wc_tcsd_message = get_option('wc-tcsd-message');
	$wc_tcsd_facebook_enabled = get_option('wc-tcsd-facebook-enabled');
	$wc_tcsd_facebook_share_enabled = get_option('wc-tcsd-facebook-share-enabled');
	$wc_tcsd_facebook_share_label = get_option('wc-tcsd-facebook-share-label');
	$wc_tcsd_facebook_colorscheme = get_option('wc-tcsd-facebook-colorscheme');
	$wc_tcsd_google_enabled = get_option('wc-tcsd-google-enabled');
	$wc_tcsd_twitter_enabled = get_option('wc-tcsd-twitter-enabled');
	$wc_tcsd_twitter_tweet_enabled = get_option('wc-tcsd-twitter-tweet-enabled');
	$wc_tcsd_twitter_username = get_option('wc-tcsd-twitter-username');
	$wc_tcsd_twitter_count = get_option('wc-tcsd-twitter-count');
	if($wc_tcsd_twitter_count == 'CURRENT'){$wc_tcsd_twitter_count = wc_tcsd_current_page(2);}
	$wc_tcsd_linkedin_enabled = get_option('wc-tcsd-linkedin-enabled');
	
	// Cache Message Vars + Default to Empty
	$followset = '';

	// Configure Pages
	$lockerPages = array(
		// Store Pages
		'facebook-store'			=> $wc_tcsc_options['facebook-url'],
		'facebook-share-store'		=> $wc_tcsc_options['facebook-share-url'],
		'google-store'				=> $wc_tcsc_options['google-url'],
		'twitter-store'				=> $wc_tcsc_options['twitter-url'],
		'linkedin-store'			=> $wc_tcsc_options['linkedin-url'],
		'vk-store'					=> $wc_tcsc_options['vk-url'],
		// Product Pages
		'facebook-products'			=> $wc_tcsc_options['facebook-url-products'],
		'facebook-share-products'	=> $wc_tcsc_options['facebook-share-url-products'],
		'google-products'			=> $wc_tcsc_options['google-url-products'],
		'twitter-products'			=> $wc_tcsc_options['twitter-url-products'],
		'linkedin-products'			=> $wc_tcsc_options['linkedin-url-products'],
		'vk-products'				=> $wc_tcsc_options['vk-url-products']
	);
	if($type == 'product-page'){$urlSwitch = '-products';} else {$urlSwitch = '-store';}
	foreach($lockerPages as $page => $value){if($value == 'CURRENT' || $value == ''){$lockerPages[$page] = wc_tcsd_current_page(2);}}
	
	// Configure Button Types
	if($wc_tcsd_button_layout == 'bubble'){
		$fbl = 'button_count';
		$gpl = 'medium';
		$twl = 'horizontal';
		$lil = 'right';
	} else if($wc_tcsd_button_layout == 'box'){
		$fbl = 'box_count';
		$gpl = 'tall';
		$twl = 'vertical';
		$lil = 'top';
	}
	
	// Create Buttons
	if($wc_tcsd_facebook_share_enabled == 'true'){ // Facebook Share
		$locks.= '<div class="tcsd-button share"><div class="tcsd-button-cover facebook"><div class="tcsd-button-icon"></div></div><div class="tcsd-button-inner"><a href="#share" class="tcsd-share '.$wc_tcsd_facebook_colorscheme.'" data-url="'.$lockerPages['facebook-share'.$urlSwitch].'" data-desc="'.$wc_tcsc_options['facebook-share-desc'].'" data-title="'.$wc_tcsc_options['facebook-share-title'].'" data-image="'.$wc_tcsc_options['facebook-share-image'].'"><span>'.$wc_tcsd_facebook_share_label.'</span></a></div></div>';
		$button_count++;
	}
	if($wc_tcsd_facebook_enabled == 'true'){ // Facebook Like
		$locks = $locks . '<div class="tcsd-button like"><div class="tcsd-button-cover facebook"><div class="tcsd-button-icon"></div></div><div class="tcsd-button-inner"><div id="fb'.$postid.'" class="fb-like" data-href="'.$lockerPages['facebook'.$urlSwitch].'" data-send="false" data-layout="'.$fbl.'" data-width="20" data-show-faces="false" data-colorscheme="'.$wc_tcsd_facebook_colorscheme.'"></div></div><script type="text/javascript">FB.Event.subscribe("edge.create", function(href, widget){if( typeof widget != "undefined" ){var tid = jQuery(widget).attr("id");if(href == "'.$lockerPages['facebook'.$urlSwitch].'" && tid == "fb'.$postid.'"){wc_tcsd_ajax("'.$wc_tcsc_options['discount-string'].'", "facebook-like");}} else {wc_tcsd_ajax("'.$wc_tcsc_options['discount-string'].'", "facebook-like");}});</script></div>';
		$button_count++;
	}
	if($wc_tcsd_google_enabled == 'true'){ // Google
		$locks = $locks . '<div class="tcsd-button plus"><div class="tcsd-button-cover google"><div class="tcsd-button-icon"></div></div><div class="tcsd-button-inner"><div class="g-plusone" data-size="'.$gpl.'" data-callback="tcsd_g_'.$postid.'" data-href="'.$lockerPages['google'.$urlSwitch].'"></div></div><script type="text/javascript">function tcsd_g_'.$postid.'(){wc_tcsd_ajax("'.$wc_tcsc_options['discount-string'].'", "google-plus");}</script></div>';
		$button_count++;
	}
	if($wc_tcsd_twitter_tweet_enabled == 'true'){ // Tweet
		$locks = $locks . '<div class="tcsd-button tweet"><div class="tcsd-button-cover twitter"><div class="tcsd-button-icon"></div></div><div class="tcsd-button-inner"><a href="https://twitter.com/share" class="twitter-share-button" data-url="'.$lockerPages['twitter'.$urlSwitch].'" data-counturl="'.$wc_tcsd_twitter_count.'" data-text="'.$wc_tcsc_options['twitter-tweet-text'].'" data-count="'.$wc_tcsd_twitter_count.'" data-lang="'.$wc_tcsc_options['twitter-api-locale'].'">Tweet</a></div></div>';
		$button_count++;
	}
	if($wc_tcsd_twitter_enabled == 'true'){ // Follow
		// Only set if username enabled
		if($wc_tcsd_twitter_username != ''){
			$locks.= '<div class="tcsd-button follow"><div class="tcsd-button-cover twitter"><div class="tcsd-button-icon"></div></div><div class="tcsd-button-inner"><a href="https://twitter.com/'.$wc_tcsd_twitter_username.'" class="twitter-follow-button" data-show-count="false" data-lang="'.$wc_tcsc_options['twitter-api-locale'].'" data-show-screen-name="false" data-dnt="true">Follow</a></div></div>';
			$button_count++;
		} else {
			$followset = '<p>'.__('Error: Follow Button Not Shown - No Username Set In Settings.').'</p>';
		}
	}
	if($wc_tcsd_linkedin_enabled == 'true'){ // LinkedIn
		$locks = $locks . '<div class="tcsd-button linkedin"><div class="tcsd-button-cover linkedin"><div class="tcsd-button-icon"></div></div><div class="tcsd-button-inner"><script type="IN/Share" data-url="'.$lockerPages['linkedin'.$urlSwitch].'" data-counter="'.$lil.'" data-showzero="'.$lil.'" data-onsuccess="tcsd_l_'.$postid.'"></script><script type="text/javascript">function tcsd_l_'.$postid.'(){wc_tcsd_ajax("'.$wc_tcsc_options['discount-string'].'", "linkedin-share");}</script></div></div>';
		$button_count++;
	}
	if( $wc_tcsc_options['vk-enabled'] == 'true' && $wc_tcsc_options['vk-app-id'] != '' ){ // VK
		$locks.= '<div class="tcsd-button vk"><div class="tcsd-button-cover vk"><div class="tcsd-button-icon"></div></div><div id="tcsd-vk-like" class="tcsd-button-inner vk"></div><script>VK.Widgets.Like("tcsd-vk-like", { type: "button", pageTitle: "Title", pageUrl: "'.$lockerPages['vk'.$urlSwitch].'", height: "22" });</script></div>';
		$button_count++;
	}

	// Button Cover Setup
	$covers_on = 'covers-off';
	if( $wc_tcsc_options['button-covers'] == 'true' ){
		$covers_on = 'covers-on';
	}
	
	// Not For IE Becuase It Blows
	if(preg_match('/(?i)msie [2-9]/',$_SERVER['HTTP_USER_AGENT'])){
		$covers_on = 'covers-off';
	}
	
	// Create Discount Locker
	$locker = '
		<div class="tcsd '.$type.'" data-id="'.$postid.'">
			<div class="tcsd-title">'.$wc_tcsd_title.'</div>
			<div class="tcsd-message">'.$wc_tcsd_message.'</div>
			<div class="tcsd-buttons '.$type.' '.$covers_on.'">'.$locks.'<br class="tcsd-clear"></div>
			'.$followset.'
			<br class="tcsd-clear">
		</div>
	';
	
	 /*
		<div style="position:absolute;top: -9999px;">
			<form id="couponForm" method="POST" action="">
				<input type="text" value="'.$wc_tcsd_discount_code.'" id="coupon_code" class="input-text" name="coupon_code"> 
				<input type="submit" class="button" style="display:block;">
				<input type="hidden" value="Apply Coupon" name="apply_coupon" />
			</form>
		</div>
		
		*/

	// Output :)
	return $locker;
	
} // End Generate Function

/*-----------------------------------------------------------------------------------*/
/*	Include TCSD Callback
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_twyt(){
	
	global $post;
	global $wc_tcsc_options;
	global $wc_tcsc_https;
	
	// Twitter Toggle
	$twitter_enabled = 'false';
	if( $wc_tcsc_options['twitter-enabled'] == 'true' || $wc_tcsc_options['twitter-tweet-enabled'] == 'true' ){
		$twitter_enabled = 'true';
	}
	
	// VK App ID Check
	$vk_enabled = 'true';
	if( $wc_tcsc_options['vk-app-id'] == '' || empty( $wc_tcsc_options['vk-app-id'] ) ){
		$vk_enabled = 'false';
	}
	
	// Only run if plugin enabled
	if( $wc_tcsc_options['enabled'] == 'true' ){ ?>
		
		<script>
            jQuery(document).ready(function(){
                jQuery().wc_tcsd({
					// Baset Setup
                    string:"<?PHP echo $wc_tcsc_options['discount-string']; ?>",
                    expires:"<?PHP echo $wc_tcsc_options['expires']; ?>",
                    p:"<?PHP echo $post->ID; ?>",
                    wpajax:"<?PHP echo admin_url().'admin-ajax.php'; ?>",
					// Facebook Setup
                    app_id:"<?PHP echo $wc_tcsc_options['facebook-app-id']; ?>",
					// Twitter Setup
                    twitter_enabled:"<?PHP echo $twitter_enabled; ?>",
					// VK Setup
					vk_enabled:"<?php echo $vk_enabled; ?>"
                });
				jQuery("input[name='coupon_code']").keyup(function(e){var ccode = jQuery(this).val();if( ccode == '<?PHP echo $wc_tcsc_options['facebook-like-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['facebook-share-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['facebook-share-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['google-plus-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['twitter-tweet-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['twitter-follow-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['linkedin-code']; ?>' ){jQuery(this).val(' ');} else if( ccode == '<?PHP echo $wc_tcsc_options['vk-code']; ?>' ){jQuery(this).val(' ');}});
            });
        </script>
        
    <?PHP // Vkontakte MUST Be Loaded In Header
		
		if( $wc_tcsc_options['vk-enabled'] == 'true' ){ 
		
			// Only Load If Valid APP ID Set
			if( $wc_tcsc_options['vk-app-id'] != '' && !empty( $wc_tcsc_options['vk-app-id'] ) ){ ?>
			
				<script type="text/javascript" src="<?PHP echo $wc_tcsc_https; ?>//vk.com/js/api/openapi.js?96"></script>
                <script type="text/javascript">VK.init({apiId: <?PHP echo $wc_tcsc_options['vk-app-id']; ?>, onlyWidgets: true});</script>
                
            <?PHP } else { // else display warning in source
			
				echo "\n\n"."<!-- WooCommerce Social Coupon - VK Disabled - Reason: No App ID Set -->"."\n\n";
				
			} // end if app ID set
			
		} // end if VK enabled
		
	} // end if plugin enabled
	
}

/*-----------------------------------------------------------------------------------*/
/*	Disable Cookie BF Expiration
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_cookie_used(){
	
	// reset cookie
	$wc_tcsd_discount_string = get_option('wc-tcsd-discount-string');	
	$expires = get_option('wc-tcsd-discount-expires');
	if( !isset($expires) || $expires == '' ){
		$expires = 24;
	}
	setcookie($wc_tcsd_discount_string, '', time()-999999, '/');
	if($expires != '0'){setcookie($wc_tcsd_discount_string.'_used', 'used', time()+(3600*$expires), '/');}
	
}
		
/*-----------------------------------------------------------------------------------*/
/*	Capture Social Buttons
/*-----------------------------------------------------------------------------------*/

if( isset( $_POST['action'] ) && ( $_POST['action'] == 'submit' ) ) {
	
	// $_POST['apply_coupon']
	/*
	
	// Get options
	$wc_tcsd_discount_string = get_option('wc-tcsd-discount-string');
	$expires = get_option('wc-tcsd-discount-expires');
	if( !isset($expires) || $expires == '' ){
		$expires = 24;
	}
	setcookie( $wc_tcsd_discount_string, "true", time()+(3600*$expires), '/' );

	// Exit
	echo true;
	exit;
	*/

}

if( isset( $_POST['apply_coupon'] ) ){
	
	//print_r($_POST);
	
	//echo 'COUPON FORM HIT!';
	
	//die();
	
	if( $_POST['coupon_code'] == 'facebook-like' ){
		
		return false;
		
	}
	
}

/*-----------------------------------------------------------------------------------*/
/*	Load Google+ & Others With Locale
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_social_loader(){
	
	// HTTPS Check
	global $wc_tcsc_https;
	
	// Make Sure Facebook Is Rendered
	if( get_option('wc-tcsd-facebook-enabled') == 'true' ){ ?>
    
    	<script type="text/javascript">FB.XFBML.parse();</script>
        
    <?PHP }
	
	// Configure and Load i18n Google+
	if( get_option('wc-tcsd-google-enabled') == 'true' ){ ?>
		
		<script type="text/javascript">
        window.___gcfg = {lang: '<?PHP echo get_option('wc-tcsd-google-api-locale'); ?>'};
        (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = '<?PHP echo $wc_tcsc_https; ?>//apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
        </script>	
  
	  <?PHP } // end if Google+ enabled
	  
	  // LinkedIn
	  if( get_option('wc-tcsd-linkedin-enabled') == 'true' ){ ?>
      
		<script src="<?PHP echo $wc_tcsc_https; ?>//platform.linkedin.com/in.js" type="text/javascript">
			lang: <?PHP echo get_option('wc-tcsd-linkedin-api-locale'); ?>
        </script>

	<?PHP } // end if LinkedIn Enabled

}

/*-----------------------------------------------------------------------------------*/
/*	Shortcode Handle
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_shortcode($atts, $content){
	
	/* Extract variables from shortcode tag, set defaults
	extract(shortcode_atts(array(
		"VAR" => 'DEFAULT',
	), $atts)); */
	
	// Check if plugin on
	if( get_option('wc-tcsd-enabled') == 'true' ){
		
		return wc_tcsd_shortcode_insert();
		
	} else {
		
		return do_shortcode($content);
		
	}

}

/*-----------------------------------------------------------------------------------*/
/*	Add Social Coupon to Page ( Short Code )
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_shortcode_insert(){
	
	// Cookie check
	global $post;
	$thisCookie = "wc_tcsd_".$post->ID;
	$wc_tcsd_discount_string = get_option('wc-tcsd-discount-string');
	if( isset( $_COOKIE[$wc_tcsd_discount_string] ) ){
		$stringCookie = $_COOKIE[$wc_tcsd_discount_string];
	} else {
		$stringCookie = 'false';
	}
	if( isset( $_COOKIE[$thisCookie] ) ){
		$postCookie = $_COOKIE[$thisCookie];
	} else {
		$postCookie = 'false';
	}
	if( isset( $_COOKIE[$wc_tcsd_discount_string.'_used'] ) ){
		$usedCookie = $_COOKIE[$wc_tcsd_discount_string.'_used'];
	} else {
		$usedCookie = 'false';
	}
	
	// Did user already share?
	if( $wc_tcsc_options['discount-type'] == 'single' ){
		
		if( $usedCookie != 'used' ){if($stringCookie == "true" && $postCookie == "true"){
			global $woocommerce;
			$wc_tcsd_discount_code = get_option('wc-tcsd-discount-code');		
			$return = '
			<div class="tcsd product-page">
				<div class="tcsd-title">'.__("Thank You for Sharing!", "wc_tcsd").'</div>
				<div class="tcsd-message">'.__("Your discount has been activated and applied to your shopping cart. This discount will only be applied once per checkout.", "wc_tcsd").'</div>
			</div>
			';
		} else { // otherwise show roll on the buttons
			$return = wc_tcsd_generate($post->ID, 'product-page');
		}} // end if
		
	} else {
		
		$return = wc_tcsd_generate($post->ID, 'product-page');
		
	}
	
	return $return;
	
}

/*-----------------------------------------------------------------------------------*/
/*	AJAX Handle
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_ajax_handle(){
	
	// Setup Globals / Options
	global $woocommerce;
	global $wc_tcsc_options;
			
	// Check if post is valid
	if( isset( $_POST['tcstring'] ) && isset( $_POST['tcbutton'] ) ){
		
		// Is Request Valid
		if( $_POST['tcstring'] == $wc_tcsc_options['discount-string'] ){
						
			// Configure Coupon
			$thisCoupon = '';
			// Check to see if multi discount is enabled, then decide what coupon to use
			if( $wc_tcsc_options['discount-type'] == 'single' ){
				$thisCoupon = $wc_tcsc_options['discount-code'];
			} else { // Match coupon to button
				$thisButton = $_POST['tcbutton'];
				if( $thisButton == 'facebook-like' ){
					$thisCoupon = $wc_tcsc_options['facebook-like-code'];
				} elseif ( $thisButton == 'facebook-share' ){
					$thisCoupon = $wc_tcsc_options['facebook-share-code'];
				} elseif ( $thisButton == 'google-plus' ){
					$thisCoupon = $wc_tcsc_options['google-plus-code'];
				} elseif ( $thisButton == 'twitter-tweet' ){
					$thisCoupon = $wc_tcsc_options['twitter-tweet-code'];
				} elseif ( $thisButton == 'twitter-follow' ){
					$thisCoupon = $wc_tcsc_options['twitter-follow-code'];
				} elseif ( $thisButton == 'linkedin-share' ){
					$thisCoupon = $wc_tcsc_options['linkedin-code'];
				} elseif ( $thisButton == 'vk-like' ){
					$thisCoupon = $wc_tcsc_options['vk-code'];
				}
			} // end discount type check
			
			// Apply		
			$woocommerce->cart->add_discount( $thisCoupon );
			
			// Return True
			echo 'true';
			
		} else { // Else: Not a valid request
			
			echo 'false';
			
		}
		
	} else { // Else String Not Posted
	
		echo 'false';
		
	} // end if
	
	die();

}

/*-----------------------------------------------------------------------------------*/
/*	Start Running Hooks
/*-----------------------------------------------------------------------------------*/

// Start the plugin
add_action( 'init', 'wc_tcsd_init' );
// Add hook to include settings CSS
add_action( 'admin_init', 'wc_tcsd_settings_admin_css' );
// create custom plugin settings menu
add_action( 'admin_menu', 'wc_tcsd_create_menu' );

// Selective Hook If Enabled
if( get_option('wc-tcsd-enabled') == 'true' ){
	
	// Load Twitter Handle
	add_action( 'wp_head', 'wc_tcsd_twyt' );
	// Load Google_
	add_action( 'wp_footer', 'wc_tcsd_social_loader' );
	// Add To Product Pages
	add_action( get_option('wc-tcsd-product-position'), 'wc_tcsd_insert_product' );
	// Add To Cart Page
	add_action( get_option('wc-tcsd-cart-position'), 'wc_tcsd_insert_cart' );
	// Add To Checkout
	add_action( get_option('wc-tcsd-checkout-position'), 'wc_tcsd_insert_checkout' );
	// After Purchase, Update Cookie
	add_action('woocommerce_after_checkout_validation', 'wc_tcsd_cookie_used' );
	// Ajax Handles
	add_action( 'wp_ajax_nopriv_tcscajax', 'wc_tcsd_ajax_handle' );
	add_action( 'wp_ajax_tcscajax', 'wc_tcsd_ajax_handle' );
	// Add Shortcode
	add_shortcode('wc-social-coupon', 'wc_tcsd_shortcode');

}

?>