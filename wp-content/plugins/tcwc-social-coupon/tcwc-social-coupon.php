<?php 

/*
Plugin Name: Social Coupon for WooCommerce
Plugin Script: tcwc-social-coupon.php
Plugin URI: http://tyler.tc
Description: Offer users discounts off their shopping cart prices for sharing your products on various social services.
Version: 1.1.9
Author: Tyler Colwell
Author URI: http://tyler.tc

--- THIS PLUGIN AND ALL FILES INCLUDED ARE COPYRIGHT © TYLER COLWELL 2012. 
YOU MAY NOT MODIFY, RESELL, DISTRIBUTE, OR COPY THIS CODE IN ANY WAY. ---

*/

/*-----------------------------------------------------------------------------------*/
/*	Define Anything Needed
/*-----------------------------------------------------------------------------------*/

define('WC_TCSD_LOCATION', plugins_url( '', __FILE__ ) );
define('WC_TCSD_PATH', plugin_dir_path(__FILE__));
define('WC_TCSD_RELPATH', dirname( plugin_basename( __FILE__ ) ) );
define('WC_TCSD_VERSION', '1.1.9');
require_once('inc/tcf_settings_page.php');
require_once('inc/tcf_bootstrap.php');

?>