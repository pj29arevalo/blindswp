��    3      �  G   L      h     i  	   �     �  G   �  7   �     ,     B     X     e  
   k     v  3   |  
   �     �     �     �  I   �  2   2  %   e     �  b   �  E   �     D  0   V     �     �     �     �     �     �     �  +        =     L  8   `  #   �      �     �     �     	  	   $	     .	     B	     S	     \	  	   �	     �	     �	     �	  #   
  �  &
           )  ,   1  N   ^  C   �     �               '     4     C  >   J     �     �     �     �  ^   �  4   .  %   c     �  c   �  N        R  4   h  (   �     �     �     �        
        "  .   A     p     �  C   �  -   �  )        9     S     k  
   �     �     �  	   �  �   �  	   �     �     �     �     �                    
                   0                 &   #                      +         -       !         3              %          	   1       2       *       ,         /   .       '       $                                     (      "   )           An email address is required Anonymous Answer correctly to qualify Cannot publish giveaway because provided image link URLs are not valid. Cannot publish giveaway until all fields are completed. Custom field deleted. Custom field updated. Did you mean Email Embed Code Enter Enter sweepstakes and receive exclusive offers from First Name Giveaway Ended Giveaway Ends Giveaway Starts Giveaway draft updated. <a target="_blank" href="%s">Preview giveaway</a> Giveaway published. <a href="%s">View giveaway</a> Giveaway restored to revision from %s Giveaway saved. Giveaway scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview giveaway</a> Giveaway submitted. <a target="_blank" href="%s">Preview giveaway</a> Giveaway updated. Giveaway updated. <a href="%s">View giveaway</a> Incorrect answer, try again! KingSumo Giveaways M j, Y @ G:i No contestants found. No winners found. Pending Please enter your first name Powered by KingSumo Giveaways for WordPress Prizes Awarded Read official rules Slow down! Giveaway hasn't started yet. Come back later. Step 1 &mdash; Giveaway Information Step 2 &mdash; Prize Information Step 3 &mdash; Question Step 4 &mdash; Design Step 5 &mdash; Services Subscribe Unsubscribe anytime Verifying Winner You have Your number of contest entries will display once you have confirmed your email address. Please check your inbox to confirm now. confirmed email address entries entry is not affiliated with the giveaway Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-06-02 13:05-0700
PO-Revision-Date: 2017-06-10 09:03-0700
Last-Translator: 
Language-Team: French (France)
Language: fr_FR
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Loco-Source-Locale: en_GB
X-Generator: Poedit 2.0.2
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e
X-Loco-Parser: loco_parse_po
X-Poedit-SearchPath-0: .
 Une adresse mail est nécessaire Anonyme Répondez correctement pour être admissible Impossible de publier ce concours car le lien URL de l'image n'est pas valide. Impossible de publier ce concours sans tous les champs renseignés. Champ supprimé. Champ mis à jour. Vouliez-vous dire Adresse mail Code intégré Entrer Entrez les tirages au sort et recevez des offres exclusives de Prénom Concours terminé Le concours se termine Le concours commence Version du concours mis à jour. <a target=“_blank” href=“%s”>Apperçu du concours</a> Concours publié. <a href=“%s”>Voir concours</a> Giveaway retorde to révision from %s Concours sauvegardé. Concours prévu pour <strong>%1$s</strong>. <a target="_blank" href="%2$s">Apperçu du concours</a> Concours soumis. <a target=“_blank” href=“%s”>Apperçu du concours</a> Concours mis à jour. Concours mis à jour. <a href="%s">Voir concours</a> Réponse incorrecte, essayez à nouveau! KingSumo Concours M j, Y @ G:i Aucun participant trouvé. Aucun gagnant trouvé. En attente Veuillez écrire votre prénom Propulsé par KingSumo Concours pour WordPress Prix décernés Lire les règles officielles Doucement! Le concours n'a pas encore commencé. Revenez plus tard. Étape 1 &mdash; Informations sur le concours Étape 2 &mdash; Informations sur le prix Étape 3 &mdash; Question Étape 4 &mdash; Design Étape 5 &mdash; Services S'inscrire Désinscription à tout moment Vérification du gagnant Vous avez Votre nombre d'entrées au concours s'affichera une fois que vous aurez confirmé votre adresse électronique. Veuillez vérifier votre boîte de réception pour confirmer dès à présent. confirmé adresse mail entrées entrée n'est pas affilié au concours 