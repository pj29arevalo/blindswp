<?php

/**
 * Plugin Name:       WooCommerce ZipMoney Payments
 * Plugin URI:        http://www.zipmoney.com.au/
 * Description:       Buy Now Pay Later. ZipMoney Payment Gateway allows realtime credit to customers in a seamless and user friendly way – all without the need to exchange any financial information, and leverage the power of promotional finance to increase sales for our business partners, risk free.
 * Version:           1.1.0
 * Author:            ZipMoney Payments
 * Author URI:        http://www.zipmoney.com.au/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Github URI:            https://github.com/zipMoney/woocommerce/
 *
 *
 * @version  1.1.0
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

$zWidgetsRendererObj = null;

/**
 * Init the plugin.
 */
add_action('plugins_loaded', 'run_WC_ZipMoney_payment_gateway');

function run_WC_ZipMoney_payment_gateway()
{
    global $zWidgetsRendererObj;

    add_action('init', array('WC_ZipMoney_Payment', 'init'));
    add_filter('woocommerce_payment_gateways', 'add_WC_ZipMoney_payment');

    /**
     * Tell WooCommerce that ZipMoney class exists
     */
    function add_WC_ZipMoney_payment($methods)
    {
        $methods[] = 'WC_ZipMoney_Payment';
        return $methods;
    }

    /**
     * Remove widgets from the action hook list
     */
    function remove_default_widgets()
    {
        global $zWidgetsRendererObj;

        // If Express Checkout Enabled
        remove_action('woocommerce_after_add_to_cart_button', array($zWidgetsRendererObj, 'render_express_payment_button'));
        remove_action('woocommerce_proceed_to_checkout', array($zWidgetsRendererObj, 'render_express_payment_button'), 30);
        remove_action('woocommerce_after_add_to_cart_button', array($zWidgetsRendererObj, 'render_widget_general'));
        remove_action('woocommerce_proceed_to_checkout', array($zWidgetsRendererObj, 'render_widget_general'), 30);
    }


    /**
     * Remove banners from the action hook list
     */
    function remove_default_banners()
    {
        global $zWidgetsRendererObj;

        remove_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_banner_shop'));
        remove_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_banner_productpage'));
        remove_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_banner_category'));
        remove_action('woocommerce_before_cart', array($zWidgetsRendererObj, 'render_banner_cart'));

    }

    if (!class_exists('WC_Payment_Gateway')) return;

    /**
     * ZipMoney  class
     */
    class WC_ZipMoney_Payment extends WC_Payment_Gateway
    {

        protected static $_instance = null;

        public $widget_express_rendered = 0;


        public static $zip_order_status = array('wc-zip-authorised' => 'Authorised',
            'wc-zip-under-review' => 'Under Review');

        private $_logo_src_url = "http://d3k1w8lx8mqizo.cloudfront.net/logo/25px/";

        public function __construct()
        {
            global $zWidgetsRendererObj;

            $this->id = 'zipmoney';
            $this->has_fields = false;
            $this->supports = array('products', 'refunds');
            $this->order_button_text = __('Proceed to zipMoney', 'woocommerce');
            add_action("wp_ajax_get_redirect_url", array("WC_ZipMoney_Express", 'get_redirect_url'));

            // Load the form fields
            $this->init_form_fields();
            // Load the settings
            $this->init_settings();
            // Init Hooks and Filters
            $this->_init_hooks();
        }

        /**
         * Return the self instance if exists
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * Registers all the hooks required during instantiation
         */
        private function _init_hooks()
        {
            global $wp_actions, $zWidgetsRendererObj;

            if (!$zWidgetsRendererObj instanceof WC_ZipMoney_Widgets_Renderer)
                $zWidgetsRendererObj = new WC_ZipMoney_Widgets_Renderer($this);

            add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));

            add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));

            // Order Actions and Status
            add_filter('wc_order_statuses', array('WC_ZipMoney_Payment', 'add_zipmoney_to_order_statuses'));
            add_filter('woocommerce_admin_order_actions', array('WC_ZipMoney_Payment', 'woocommerce_admin_order_actions'), 10, 2);
            add_filter('woocommerce_valid_order_statuses_for_payment_complete', array('WC_ZipMoney_Payment', 'woocommerce_valid_order_statuses_for_payment_complete'), 10, 1);
            add_filter('woocommerce_gateway_description', array($zWidgetsRendererObj, 'updateMethodDescription'), 10, 2);

            if ($this->is_iframe_flow && $this->is_available())
                add_filter('woocommerce_order_button_html', array($zWidgetsRendererObj, 'order_button'), 10, 2);

            // Hooks
            add_action('admin_notices', array($this, 'checks'));
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_config'));

            add_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_root_el'));
            add_action('woocommerce_before_cart', array($zWidgetsRendererObj, 'render_root_el'));
            add_action('woocommerce_before_checkout_form', array($zWidgetsRendererObj, 'render_root_el'));

            if ($this->_is_banner_enabled('shop'))
                add_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_banner_shop'));

            if ($this->_is_banner_enabled('productpage'))
                add_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_banner_productpage'));

            if ($this->_is_banner_enabled('category'))
                add_action('woocommerce_before_main_content', array($zWidgetsRendererObj, 'render_banner_category'));

            if ($this->_is_banner_enabled('cart'))
                add_action('woocommerce_before_cart', array($zWidgetsRendererObj, 'render_banner_cart'));

            // For customised template
            if ($this->display_tagline_productpage)
                add_action('woocommerce_single_product_summary', array($zWidgetsRendererObj, 'render_tagline'));

            if ($this->display_tagline_cart) {
                add_action('woocommerce_proceed_to_checkout', array($zWidgetsRendererObj, 'render_tagline'), 10);
            }


            // For customised template
            if ($this->is_express)
                add_action('zipmoney_wc_render_widget_general', array($zWidgetsRendererObj, 'render_express_payment_button'), 12);
            elseif ($this->display_widget)
                add_action('zipmoney_wc_render_widget_general', array($zWidgetsRendererObj, 'render_widget_general'), 10);


            // Express Checkout or Widget Product Page
            if ($this->_is_express_enabled('productpage'))
                add_action('woocommerce_after_add_to_cart_button', array($zWidgetsRendererObj, 'render_express_payment_button'));
            else if ($this->_is_widget_enabled('productpage'))
                add_action('woocommerce_after_add_to_cart_button', array($zWidgetsRendererObj, 'render_widget_product'));

            // Express Checkout or Widget Cart
            if ($this->_is_express_enabled('cart'))
                add_action('woocommerce_proceed_to_checkout', array($zWidgetsRendererObj, 'render_express_payment_button'), 20);
            else if ($this->_is_widget_enabled('cart'))
                add_action('woocommerce_proceed_to_checkout', array($zWidgetsRendererObj, 'render_widget_cart'), 20);
        }

        /**
         *  Retrieve all the settings and configs
         */
        public function init_settings()
        {
            parent::init_settings();

            // Get setting values
            $this->enabled = 'yes' === $this->get_option('enabled', 'no');
            $this->sandbox = 'yes' === $this->get_option('sandbox', 'no', 'no');


            $this->debug = 'yes' === $this->get_option('debug', 'no');
            $this->is_iframe_flow = 'yes' === $this->get_option('is_iframe_flow', 'no');

            // Flag that identifies whether the merchant keys got updated
            $this->api_hash = get_option($this->plugin_id . $this->id . '_api_hash', true);

            // Load the api settings
            $this->api_settings = get_option($this->plugin_id . $this->id . '_api_settings', true);


            $this->icon = $this->_get_method_icon();

            // Admin Title and Description
            $this->method_title = "ZipMoney";
            $this->method_description = "Shop Interest Free - get a decision in seconds!";

            //Front Title and Description
            $this->title = isset($this->api_settings['checkout_title']) ? $this->api_settings['checkout_title'] : $this->method_title;
            $this->description = $this->_get_method_description();

            // Widgets
            $this->display_widget = 'yes' === $this->get_option('display_widget', 'no');
            $this->display_widget_productpage = 'yes' === $this->get_option('display_widget_productpage', 'no');
            $this->display_widget_cart = 'yes' === $this->get_option('display_widget_cart');

            // Express Checkout
            $this->is_express = 'yes' === $this->get_option('is_express', 'no');
            $this->is_express_productpage = 'yes' === $this->get_option('is_express_productpage', 'no');
            $this->is_express_cart = 'yes' === $this->get_option('is_express_cart', 'no');

            // Banners
            $this->display_banners = 'yes' === $this->get_option('display_banners', 'no');
            $this->display_banner_homepage = 'yes' === $this->get_option('display_banner_homepage', 'no');
            $this->display_banner_shop = 'yes' === $this->get_option('display_banner_shop', 'no');
            $this->display_banner_productpage = 'yes' === $this->get_option('display_banner_productpage', 'no');
            $this->display_banner_category = 'yes' === $this->get_option('display_banner_category', 'no');
            $this->display_banner_cart = 'yes' === $this->get_option('display_banner_cart', 'no');

            $this->display_tagline_productpage = 'yes' === $this->get_option('display_tagline_productpage', 'no');
            $this->display_tagline_cart = 'yes' === $this->get_option('display_tagline_cart', 'no');


            $this->environment = $this->sandbox ? 'sandbox' : 'production';
            $this->merchant_id = $this->sandbox ? $this->get_option('sandbox_merchant_id') : $this->get_option('merchant_id');
            $this->merchant_key = $this->sandbox ? $this->get_option('sandbox_merchant_key') : $this->get_option('merchant_key');

            $this->merchant_public_key = $this->sandbox ? $this->get_option('sandbox_merchant_public_key') : $this->get_option('merchant_public_key');
        }


        /**
         * Init method
         */
        public static function init()
        {
            //Rewrite Rules
            self::_add_rewrite_rules();

            //Order Status
            self::_register_order_status();

            //Register Post Type 'shop_quote'
            self::_register_quote();

            //Include SDK and Plugin Files
            self::_init_zipmoney_sdk();

            add_filter('query_vars', array('WC_ZipMoney_Payment', 'add_query_vars_filter'));

            add_action('parse_request', array('WC_ZipMoney_Payment', 'process_zipmoney_request'));

        }


        /**
         * Admin Panel Options
         */
        public function admin_options()
        {
            ?>
            <h3><?php _e('ZipMoney', 'woocommerce'); ?></h3>
            <p><?php _e('ZipMoney Payments allows realtime credit to customers in a seamless and user friendly way.', 'woocommerce'); ?></p>
            <table class="form-table">
                <?php $this->generate_settings_html(); ?>
                <script type="text/javascript">

                    jQuery('#woocommerce_zipmoney_sandbox').change(function () {
                        var sandbox = jQuery('#woocommerce_zipmoney_sandbox_merchant_id, #woocommerce_zipmoney_sandbox_merchant_key').closest('tr');
                        production = jQuery('#woocommerce_zipmoney_merchant_id, #woocommerce_zipmoney_merchant_key').closest('tr');

                        if (jQuery(this).is(':checked')) {
                            sandbox.show();
                            production.hide();
                        } else {
                            sandbox.hide();
                            production.show();
                        }

                    }).change();

                    jQuery('#woocommerce_zipmoney_display_banners').change(function () {

                        var banner_settings = jQuery('#woocommerce_zipmoney_display_banner_shop, #woocommerce_zipmoney_display_banner_productpage, #woocommerce_zipmoney_display_banner_category, #woocommerce_zipmoney_display_banner_cart');
                        var banner_settings_tr = banner_settings.closest('tr');

                        if (jQuery(this).is(':checked')) {
                            banner_settings_tr.show();
                        } else {
                            banner_settings_tr.hide();
                        }

                    }).change();

                    jQuery('#woocommerce_zipmoney_display_widget').change(function () {

                        var banner_settings = jQuery('#woocommerce_zipmoney_display_widget_productpage, #woocommerce_zipmoney_display_widget_cart');
                        var banner_settings_tr = banner_settings.closest('tr');

                        if (jQuery(this).is(':checked')) {
                            banner_settings_tr.show();
                        } else {
                            banner_settings_tr.hide();
                        }

                    }).change();

                    jQuery('#woocommerce_zipmoney_is_express').change(function () {

                        var banner_settings = jQuery('#woocommerce_zipmoney_is_express_productpage, #woocommerce_zipmoney_is_express_cart');
                        var banner_settings_tr = banner_settings.closest('tr');

                        if (jQuery(this).is(':checked')) {
                            banner_settings_tr.show();
                        } else {
                            banner_settings_tr.hide();
                        }

                    }).change();

                </script>
            </table> <?php
        }

        /**
         * Check if SSL is enabled and notify the user
         */
        public function checks()
        {
            if (!$this->enabled) {
                return;
            }

            // PHP Version
            if (version_compare(phpversion(), '5.2.1', '<')) {
                echo '<div class="error"><p>' . sprintf(__('ZipMoney Error: ZipMoney requires PHP 5.3 and above. You are using version %s.', 'woocommerce'), phpversion()) . '</p></div>';
            } // Show message if enabled and FORCE SSL is disabled and WordpressHTTPS plugin is not detected
            elseif ('no' == get_option('woocommerce_force_ssl_checkout') && !class_exists('WordPressHTTPS')) {
                echo '<div class="error"><p>' . sprintf(__('ZipMoney is enabled, but the <a href="%s">force SSL option</a> is disabled; your checkout may not be secure! Please enable SSL and ensure your server has a valid SSL certificate - ZipMoney will only work in sandbox mode.', 'woocommerce'), admin_url('admin.php?page=wc-settings&tab=checkout')) . '</p></div>';
            }
        }

        /**
         * Check if this gateway is enabled
         */
        public function is_available()
        {
            if (!$this->enabled) {
                return false;
            }

            if (!is_ssl() && !$this->sandbox) {
                return false;
            }

            return true;
        }

        /**
         *  Returns the payment method icon based on product classification
         */
        private function _get_method_icon()
        {
            $logo = isset($this->api_settings['product']) ? $this->api_settings['product'] : 'zipmoney';

            return apply_filters('woocommerce_zipmoney_icon', $this->_logo_src_url . strtolower($logo) . '.png');
        }

        /**
         *  Returns the payment method description
         */
        private function _get_method_description()
        {
            return isset($this->api_settings['checkout_description']) ? $this->api_settings['checkout_description'] : $this->method_description;
        }

        /**
         *  Checks if the banner is enabled for the given page type
         */
        private function _is_banner_enabled($type)
        {
            $prop = "display_banner_" . $type;

            return $this->display_banners && $this->{$prop};
        }

        /**
         *  Checks if the widget is enabled for the given page type
         */
        private function _is_widget_enabled($type)
        {
            $prop = "display_widget_" . $type;

            return $this->display_widget && $this->{$prop};
        }

        /**
         *  Checks if the express button is enabled for the given page type
         */
        private function _is_express_enabled($type)
        {
            $prop = "is_express_" . $type;

            return $this->is_express && $this->{$prop};
        }

        /**
         * Initialise Gateway Settings Form Fields
         */
        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'label' => __('Enable ZipMoney Payment', 'woocommerce'),
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'sandbox' => array(
                    'title' => __('Sandbox', 'woocommerce'),
                    'label' => __('Enable Sandbox Mode', 'woocommerce'),
                    'type' => 'checkbox',
                    'desc_tip' => __('Place the payment gateway in sandbox mode using sandbox API credentials for testing.', 'woocommerce'),
                    'default' => 'no'
                ),
                'sandbox_merchant_id' => array(
                    'title' => __('Sandbox Merchant ID', 'woocommerce'),
                    'type' => 'text',
                    'desc_tip' => __('Get your Sandbox Merchant ID from your zipMoney account.', 'woocommerce'),
                    'default' => '',
                ),

                'sandbox_merchant_key' => array(
                    'title' => __('Sandbox Merchant Key', 'woocommerce'),
                    'type' => 'text',
                    'desc_tip' => __('Get your Sandbox Merchant Key from your zipMoney account.', 'woocommerce'),
                    'default' => '',
                ),
                'merchant_id' => array(
                    'title' => __('Merchant ID', 'woocommerce'),
                    'type' => 'text',
                    'desc_tip' => __('Get your Merchant ID from your zipMoney account.', 'woocommerce'),
                    'default' => '',
                ),
                'merchant_key' => array(
                    'title' => __('Merchant Key', 'woocommerce'),
                    'type' => 'text',
                    'desc_tip' => __('Get your Merchant Key from your zpMoney account.', 'woocommerce'),
                    'default' => '',
                ),
                'debug' => array(
                    'title' => __('Debug', 'woocommerce'),
                    'label' => __('Enable logging', 'woocommerce'),
                    'type' => 'checkbox',
                    'desc_tip' => __('Enables logging.', 'woocommerce'),
                    'default' => 'no'
                ),
                'is_express' => array(
                    'title' => __('Express Checkout', 'woocommerce'),
                    'label' => __('Enable express checkout.', 'woocommerce'),
                    'type' => 'checkbox',
                    'desc_tip' => __('Enables express checkout on product & cart page.', 'woocommerce'),
                    'default' => 'no'
                ),
                'is_express_productpage' => array(
                    'label' => __('Enable express checkout on product page.', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'is_express_cart' => array(
                    'label' => __('Enable express checkout on cart.', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'is_iframe_flow' => array(
                    'title' => __('Iframe Checkout', 'woocommerce'),
                    'label' => __('Enable Iframe Checkout Flow.', 'woocommerce'),
                    'type' => 'checkbox',
                    'description' => __('In-context checkout flow without leaving the store. In order for this to work on product and cart pages, express checkout has to be turned on for those pages.', 'woocommerce'),
                    'default' => 'no'
                ),

                'display_widget' => array(
                    'title' => __('Marketing Widgets', 'woocommerce'),
                    'label' => __('Display Marketing Widgets', 'woocommerce'),
                    'type' => 'checkbox',
                    'desc_tip' => __('Enables the display of marketing widgets below the add to cart and checkout button.', 'woocommerce'),
                    'default' => 'yes'
                ),
                'display_widget_productpage' => array(
                    'label' => __('Display on product page', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'yes'
                ),
                'display_widget_cart' => array(
                    'label' => __('Display on cart page', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'yes'
                ),

                'display_banners' => array(
                    'title' => __('Marketing Banners', 'woocommerce'),
                    'label' => __('Display Marketing Banners', 'woocommerce'),
                    'type' => 'checkbox',
                    'desc_tip' => __('Enables the display of marketing banners in the site.', 'woocommerce'),
                    'default' => 'no'
                ),
                'display_banner_shop' => array(
                    'label' => __('Display on Shop', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'display_banner_productpage' => array(
                    'label' => __('Display on Product Page', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'display_banner_category' => array(
                    'label' => __('Display on Category Page', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'display_banner_cart' => array(
                    'label' => __('Display on Cart', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'display_tagline_productpage' => array(
                    'title' => __('Tagline', 'woocommerce'),
                    'label' => __('Display on product page', 'woocommerce'),
                    'desc_tip' => __('Enables the display of tagline widgets below the price in product page.', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'yes'
                ),
                'display_tagline_cart' => array(
                    'label' => __('Display on cart page', 'woocommerce'),
                    'desc_tip' => __('Enables the display of tagline widgets below grand total in cart page.', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'yes'
                ),
            );
        }

        /**
         * Outputs style used for ZipMoney Payment admin section
         */
        public function admin_scripts()
        {
            wp_register_style('wc-zipmoney-style-admin', plugin_dir_url(__FILE__) . 'assets/css/woocommerce-zipmoney-payment-admin.css', array(), '20151118', 'all');
            wp_enqueue_style('wc-zipmoney-style-admin');

        }

        /**
         * Register style and scripts required
         */
        public function payment_scripts()
        {

            wp_register_style('wc-zipmoney-style', plugin_dir_url(__FILE__) . 'assets/css/woocommerce-zipmoney-payment-front.css');
            wp_enqueue_style('wc-zipmoney-style');

            wp_register_script('wc-zipmoney-script', plugin_dir_url(__FILE__) . 'assets/js/woocommerce-zipmoney-payment-front.js', array('thickbox'), '2.1.1', true);

            wp_register_script('wc-zipmoney-widget-js', 'https://d3k1w8lx8mqizo.cloudfront.net/lib/js/zm-widget-js/dist/zipmoney-widgets-v1.min.js', array('jquery'), '1.0.5', true);

            if ($this->sandbox)
                $iframe_lib_url = WC_ZipMoney_Express::IFRAME_API_URL_TEST;
            else
                $iframe_lib_url = WC_ZipMoney_Express::IFRAME_API_URL_LIVE;

            wp_register_script('wc-zipmoney-js', $iframe_lib_url);

            wp_enqueue_script('wc-zipmoney-script');
            wp_enqueue_script('wc-zipmoney-widget-js');
            wp_enqueue_script('wc-zipmoney-js');
        }


        /**
         * Saves the zipMoney Configs
         */
        public function process_admin_config()
        {
            $environment = $this->sandbox == 'no' ? 'production' : 'sandbox';
            $merchant_id = $this->sandbox == 'no' ? $this->get_option('merchant_id') : $this->get_option('sandbox_merchant_id');
            $merchant_key = $this->sandbox == 'no' ? $this->get_option('merchant_key') : $this->get_option('sandbox_merchant_key');

            $hash = md5(serialize(array($this->merchant_id, $this->merchant_key)));

            if ($hash !== $this->api_hash) {
                // update config in zipmoney
                update_option($this->plugin_id . $this->id . '_api_hash', $hash);

                try {

                    WC_ZipMoney::staticLog("ZipMoney Config Update Initiated", $this->debug);

                    $gateway = self::instance();
                    $wc_zipmoneyObj = new WC_ZipMoney($gateway);
                    $wc_zipmoneyObj->handleConfigUpdate();

                } catch (Exception $e) {
                    return false;
                }
            }
        }

        /**
         * Processes the refund
         */
        public function process_refund($order_id, $amount = null, $reason = "Refund")
        {
            $payment_id = get_post_meta($order_id, '_transaction_id', true);

            $order = new WC_Order($order_id);

            WC_ZipMoney::staticLog("ZipMoney Refund Initiated");

            $wc_zipmoneyObj = new WC_ZipMoney_Standard($this);

            if(empty($reason)) $reason = "Refund";

            $response = $wc_zipmoneyObj->handleRefund($order, $amount, $reason);

            if ($response->status == 'Refunded')
                return true;

            return false;
        }


        /**
         * Processes the payment
         */
        public function process_payment($order_id)
        {
            global $woocommerce;

            $order = new WC_Order($order_id);

            WC_ZipMoney::staticLog("ZipMoney Payment Initiated", $this->debug);

            try {
                $wc_zipmoneyObj = new WC_ZipMoney_Standard($this);

                if ($redirect_url = $wc_zipmoneyObj->handleCheckout($order)) {

                    $order->add_order_note(sprintf("Resume Application Url:-%s", $redirect_url));

                    WC_ZipMoney::staticLog("Redirecting to ZipMoney", $this->debug);

                }

                return array(
                    'result' => 'success',
                    'redirect' => $redirect_url
                );
            } catch (Exception $e) {
                WC_ZipMoney::staticLog($e->getMessage(), $this->debug);
            }

            return false;
        }

        /**
         * Entry points for WebHooks
         */
        public static function process_zipmoney_request($wp)
        {
            global $woocommerce;

            $query_vars = $wp->query_vars;

            $allowedMethodsAndRoutes = array("subscribe", 'shippingdetails', 'shippingmethods', 'quotedetails', 'confirmorder', "expresscheckout", "standardcheckout", "express", "getredirecturl", "confirmshippingmethod", "error");

            if ((isset($query_vars['p']) && $query_vars['p'] != "zipmoneypayment") ||
                (isset($query_vars['method']) && !empty($query_vars['method']) && !in_array($query_vars['method'], $allowedMethodsAndRoutes)) ||
                (isset($query_vars['action_type']) && !empty($query_vars['action_type']) && !in_array($query_vars['action_type'], $allowedMethodsAndRoutes))
            )
                return false;

            $gateway = self::instance();

            if (!isset($query_vars['route']))
                return;


            try {

                switch ($query_vars['route']) {

                    case 'subscribe': // Handle the subcribe webhook

                        $zWebHookObj = new WC_ZipMoney_WebHook($gateway->merchant_id, $gateway->merchant_key, $gateway->debug);
                        $zWebHookObj->listen();

                        break;
                    case 'expresscheckout':  // Checkout from merchant's store to zipmoney

                        $wc_expressObj = new WC_ZipMoney_Express($gateway);

                        if (!$query_vars['action_type']) {

                            if ($redirect_url = $wc_expressObj->handleExpressCheckout()) {
                                wp_redirect($redirect_url);
                                die();
                            }
                        } else if ($query_vars['action_type']) {
                            call_user_func(array($wc_expressObj, $query_vars['action_type']));
                            die();
                        }

                        break;

                    case 'standardcheckout':  // Checkout from merchant's store to zipmoney

                        $wc_standardObj = new WC_ZipMoney_Standard($gateway);
                        if ($query_vars['action_type']) {
                            call_user_func(array($wc_standardObj, $query_vars['action_type']));
                            die();
                        }

                        break;

                    case 'express':

                        $zExpressWebHookObj = new WC_ZipMoney_ExpressWebHook($gateway);
                        $zExpressWebHookObj->listen($query_vars['action_type']);

                    case 'error':

                        $zObj = new WC_ZipMoney($gateway);
                        $zObj->error();

                        break;
                }

            } catch (Exception $e) {
                WC_ZipMoney::staticLog($e->getMessage(), $gateway->debug);
            }

            die();
        }


        /**
         * Adds query vars
         */
        public static function add_query_vars_filter($vars)
        {
            $vars[] = "checkout_source";
            $vars[] = "product_id";

            return $vars;
        }

        /**
         * Returns error url
         */
        public function get_error_url()
        {
            return get_home_url() . "/zipmoneypayment/error/";
        }

        /**
         *  Add to list of WC Order statuses
         */
        public static function add_zipmoney_to_order_statuses($order_statuses)
        {

            foreach (self::$zip_order_status as $index => $status)
                $order_statuses[$index] = $status;

            return $order_statuses;
        }

        /**
         *  Adds some url rewrite for webhook urls.
         */
        private static function _add_rewrite_rules()
        {
            // Define the tag for the individual ID
            add_rewrite_tag('%route%', '([a-zA-Z]*)');
            add_rewrite_tag('%action_type%', '([a-zA-Z]*)');
            add_rewrite_rule('^zipmoneypayment/([a-zA-Z]*)/?([a-zA-Z]*)', 'index.php?p=zipmoneypayment&route=$matches[1]&action_type=$matches[2]', 'top');

            flush_rewrite_rules();
        }

        /**
         *  Registers custom order status
         */
        private static function _register_order_status()
        {
            // Register zipmoney order statuses
            foreach (self::$zip_order_status as $index => $status) {
                register_post_status($index, array(
                    'label' => $status,
                    'public' => true,
                    'exclude_from_search' => false,
                    'show_in_admin_all_list' => true,
                    'show_in_admin_status_list' => true,
                    'label_count' => _n_noop($status . ' <span class="count">(%s)</span>', $status . ' <span class="count">(%s)</span>')
                ));
            }
        }

        /**
         *  Registers custom post type required for Express Checkout Flow
         */
        private static function _register_quote()
        {
            // Register shop_quote post types
            register_post_type('shop_quote',
                array(
                    'labels' => array(
                        'name' => __('Quote'),
                        'singular_name' => __('Quote')
                    ),
                    'public' => false,
                    'has_archive' => true
                )
            );
        }

        /**
         *  Includes all the zipmoney sdk and plugins files
         */
        private static function _init_zipmoney_sdk()
        {
            // Include SDK bootstrap
            require_once "includes/zipmoney-api/ZipMoney.php";

            // Include Plugin Files
            require_once "includes/wc-zipmoney.php";
            require_once "includes/wc-zipmoney-standard.php";
            require_once "includes/wc-zipmoney-express.php";
            require_once "includes/wc-zipmoney-webhook.php";
            require_once "includes/wc-zipmoney-widgets.php";

        }

        /**
         *  Adds zipmoney specific orders actions for orders in admin
         */
        public function woocommerce_admin_order_actions($actions, $the_order)
        {
            $actions = array();
            if ($the_order->has_status(array('zip-authorised', 'pending', 'on-hold'))) {
                $actions['processing'] = array(
                    'url' => wp_nonce_url(admin_url('admin-ajax.php?action=woocommerce_mark_order_status&status=processing&order_id=' . $the_order->id), 'woocommerce-mark-order-status'),
                    'name' => __('Processing', 'woocommerce'),
                    'action' => "processing"
                );
            }

            if ($the_order->has_status(array('zip-authorised', 'pending', 'on-hold', 'processing'))) {
                $actions['complete'] = array(
                    'url' => wp_nonce_url(admin_url('admin-ajax.php?action=woocommerce_mark_order_status&status=completed&order_id=' . $the_order->id), 'woocommerce-mark-order-status'),
                    'name' => __('Complete', 'woocommerce'),
                    'action' => "complete"
                );
            }

            $actions['view'] = array(
                'url' => admin_url('post.php?post=' . $the_order->id . '&action=edit'),
                'name' => __('View', 'woocommerce'),
                'action' => "view"
            );


            return $actions;
        }

        /**
         *  Adds zipmoney specific orders status
         */
        public function woocommerce_valid_order_statuses_for_payment_complete($order_statuses)
        {
            $order_statuses[] = 'zip-authorised';

            return $order_statuses;
        }
    }
}
