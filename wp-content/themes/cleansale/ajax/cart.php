<?php function ocmx_cart_display(){
	global $woocommerce; ?>
	<div class="header-cart">
    	<a class="cart">
			<?php
                //echo sprintf(_n('%d item &ndash; ', '%d items &ndash; ', $woocommerce->cart->cart_contents_count), $woocommerce->cart->cart_contents_count);
				//db_write(var_export($woocommerce->cart->get_total(), true), true, 'GETTOTAL');
				$woocommerce->cart->calculate_totals();
				//db_write(print_r($woocommerce->cart, true), true, 'CART');
                echo apply_filters('woocommerce_cart_totals_order_total_html', '<strong>' . $woocommerce->cart->get_cart_subtotal(true) . '</strong>');
            ?>
    	</a>
    	<div class="shopping-cart">
    		<h3 class="widgettitle"><span><?php _e("My Cart", "ocmx");?></span></h3>
    		<?php if (sizeof($woocommerce->cart->get_cart())>0) : ?>
    			<ul>
    			<?php foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) :
    					$_product = $cart_item['data'];
						if ($_product->exists() && $cart_item['quantity']>0) :
							echo '<li class="product">';
							echo '<a href="'.get_permalink($cart_item['product_id']).'">' . apply_filters('woocommerce_cart_widget_product_title', $_product->get_title(), $_product).'</a><br />';
							echo $cart_item['quantity'].' x ';
							//echo woocommerce_price($_product->get_price()).'</li>';
							
							echo apply_filters( 'woocommerce_cart_item_subtotal', $woocommerce->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							
							echo '</li>';
						endif;
    				endforeach; ?>
    				<li class="buttons">
                        <a class="button" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" class="checkout"><?php _e("View Cart",'ocmx');?></a>
                        <a class="button last" href="<?php echo $woocommerce->cart->get_checkout_url(); ?>" class="checkout"><?php _e("Checkout",'ocmx');?></a>
    				</li>
    			</ul>
			<?php else :
            	echo '<span class="empty">'.__('No products in the cart.', 'woocommerce').'</span>';
            endif; ?>
    	</div>
    </div><!--End Header Cart -->
    <?php
     if( isset( $_REQUEST['action'] ) ) die();
} ?>