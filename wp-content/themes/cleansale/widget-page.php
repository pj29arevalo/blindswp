<?php 
/* Template Name: Widgetized Page */
get_header();
global $post; 
$widgetpage = $post->post_title;
?>
<!--Begin Page Content Area --> 
<ul class="double-cloumn clearfix">     
    <div class="full-width">
    <?php if ( have_posts()) :
        global $show_author, $post;
        $show_author = 1;
        while ( have_posts()) : the_post(); setup_postdata($post);
        $link = get_permalink( $post->ID ); 
    ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class('post-content clearfix'); ?>>
                <h2 class="page-title"><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>           
                <div class="copy <?php echo $image_class; ?> clearfix">
                     <?php the_content(""); ?>
                </div>
            </div>
        <?php endwhile;
        else :
            ocmx_no_posts();
        endif; ?> 
    </div>
    <!--Begin widgets -->
    <div id="widget-page" class="clearfix">
        <ul class="widget-list">
            <?php dynamic_sidebar($widgetpage." Body"); ?>
        </ul>
    </div>
</ul>
<?php  get_footer(); ?>