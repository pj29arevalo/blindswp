<?php
/*
Template Name: Shop 
*/

get_header();
global $product;
$term =	$wp_query->queried_object;
$_product = $product; ?>

<ul class="double-cloumn clearfix">
    <li id="left-column">	
    	<?php do_action('woocommerce_before_single_product', $post, $_product); ?>
    	<h2 class="category-title"><?php echo $term->name; ?></h2>
    	<?php if($term->description !='') : ?>
			<p class="category-description"><?php echo $term->description; ?></p>
		<?php endif; ?>
		<?php if ( have_posts() ) : ?>

			<?php do_action( 'woocommerce_before_shop_loop' ); 
					woocommerce_product_loop_start();
					woocommerce_product_subcategories(); 
					while ( have_posts() ) : the_post(); ?>

			<?php woocommerce_get_template_part( 'content', 'product' ); 
					endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php do_action( 'woocommerce_after_shop_loop' );?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : 
				woocommerce_get_template( 'loop/no-products-found.php' ); 
				endif; ?>
                
		<?php motionpic_pagination("clearfix", "pagination clearfix"); ?>
	</li>
	<?php get_sidebar(); ?>
</ul>
<?php get_footer(); ?>