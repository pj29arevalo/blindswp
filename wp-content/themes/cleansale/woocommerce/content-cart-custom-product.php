<?php

global $product, $post;

$_product = $product;
$link = get_permalink($post->ID);
$args = array('postid' => $post->ID, 'width' => 340, 'hide_href' => true, 'exclude_video' => false, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => '660auto');
$video = get_obox_media($args);
$image_option = get_option("ocmx_product_image");

$cart_item_id = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item']) ? $_POST['edit_cart_item'] : 0;
$cart_item_id = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item']) ? $_POST['edit_cart_item'] : 0;

//echo "cart_item_id: ".$cart_item_id."<br/>";

$item = WC()->cart->get_cart_item($cart_item_id);

if ($item) {
    $gravity_form_id = $item["_gravity_form_data"]["id"];
    $values = $item["_gravity_form_lead"];
    $variation_data = $item["variation"];
    $formated_price_total = sprintf(get_woocommerce_price_format(), get_woocommerce_currency_symbol(), $item["line_total"]);
}

//echo "gravity_form_id: $gravity_form_id<br/>";
//echo "<pre style='white-space: pre-wrap;'>";print_r($variation_data);echo "</pre>";
//echo "<pre style='white-space: pre-wrap;'>";print_r($values);echo "</pre>";
//echo "<pre style='white-space: pre-wrap;'>";print_r($item);echo "</pre>";

?>
<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
    <ul class="product-header clearfix">

        <li class="gallery-slider <?php if ($image_option == "gallery") : ?>gallery-option <?php else: ?>slider-option<?php endif; ?> clearfix">

            <?php if (isset($image_option) && $image_option == "slider") : ?>
                <ul class="gallery-container gallery-image clearfix">
                    <?php
                    $attach_args = array("post_type" => "attachment", "post_parent" => $post->ID, "numberposts" => "-1", "orderby" => "menu_order", "order" => "ASC");
                    $attachments = get_posts($attach_args);
                    if (!empty($attachments)) :

                        foreach ($attachments as $attachement => $this_attachment) :
                            $image = wp_get_attachment_image_src($this_attachment->ID, "660auto");
                            $full = wp_get_attachment_image_src($this_attachment->ID, "full");
                            ?>
                            <li>
                                <img src="<?php echo $image[0]; ?>" alt="<?php echo $this_attachment->post_title; ?>" />
                            </li>
                        <?php
                        endforeach;

                    else :
                        ?>
                        <li>
                            <?php echo $video; ?>
                        </li>
                    <?php endif; ?>
                </ul>

                <?php
                $counter = count($attachments);
                if (!empty($attachments) && $counter != '1') :
                    ?>

                    <div class="controls"> 
                        <a href="#" class="next"></a> 
                        <a href="#" class="previous"></a>
                    </div>
                <?php endif; ?>

            <?php else : ?>

                <?php
                do_action('woocommerce_before_single_product_summary', $post, $_product);
                ?>

            <?php endif; ?>



            <?php echo "<div class='product-custom-content'>" . $post->post_content . "</div>"; ?>

        </li><!--End Slider container -->

        <li class="product-content">
            <?php do_action('woocommerce_single_product_summary'); ?> 
            <?php if (get_option("ocmx_meta_social") != "false" && false) : ?>
                <ul class="social">
                    <li class="addthis">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style ">
                            <a class="addthis_button_facebook_like"></a>
                            <a class="addthis_button_tweet"></a>
                            <a class="addthis_counter addthis_pill_style"></a>
                        </div>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-507462e4620a0fff"></script>
                        <!-- AddThis Button END -->
                    </li>
                </ul>
            <?php endif; ?>
        </li>

    </ul>
</div>


<script type="text/javascript">
	var edit_cart_config = {
		'form_id' : '<?php echo $gravity_form_id; ?>',
		'values' : <?php echo json_encode($values); ?>,
		'variation_data' : <?php echo JSON_encode($variation_data); ?>,
		'formated_price_total' : '<?php echo $formated_price_total; ?>'
	};
	
	jQuery(document).ready(function ($) {
		$('.single_add_to_cart_button').html('Save');
		$('<input>', {
			'type'  : 'hidden',
			'name'  : 'edit_cart_item',
			'value' : '<?php echo $cart_item_id; ?>'
		}).appendTo('.variations_form');
	});
</script>

