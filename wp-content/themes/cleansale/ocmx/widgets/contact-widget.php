<?php
class contact_widget extends WP_Widget {
	function __construct() {
			$widget_ops = array( 'classname' => 'obox_contact_widget', 'description' => 'List your contact details here to make it easy for sponsors, prospects or organizers to contact you.' );
			parent::__construct( 'contact_widget', '(Obox) Contact Details', $widget_ops );
    }
   	/** @see WP_Widget::widget */
	function widget($args, $instance) {
		// Turn $instance array into variables
		$instance_args = wp_parse_args( $instance);
		extract( $instance_args, EXTR_SKIP );
		?>
        
        <li class="column contact-widget">
			<h4>
				<?php if($page_choose != 0) :  ?>
					<a href="<?php echo get_page_link($page_choose); ?>">
						<?php echo $title; ?>
					</a>
				<?php else : ?>
					<?php echo $title; ?>
				<?php endif; ?>
			</h4>
			
			<ul class="clearfix">
				<li>
					<div class="details phone-icon">
						<?php if(isset($phone_link) == "on") : ?>
							<a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
						<?php else: ?>
							<?php echo $phone; ?>
						<?php endif; ?>
					</div>
				</li>
				<li>
					<div class="details email-icon">
						<?php if(isset($email_link) == "on") : ?>
							<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
						<?php else: ?>
							<?php echo $email; ?>
						<?php endif; ?>
					</div>
				</li>
			</ul>
			
		</li>
		
	<?php		
    }

 	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		// Turn $instance array into variables
		$instance_defaults = array ( 'title' => 'Contact Us', 'phone' => 'i.e. +1 123 456 7890', 'email' => 'i.e. you@youremail.com');
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP ); ?>	
        	<p>
        		<label for="<?php echo $this->get_field_id('title'); ?>"> <?php _e('Title', 'ocmx'); ?><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label>
        	</p>
        	<p>
        		<label for="<?php echo $this->get_field_id('phone'); ?>"> <?php _e('Phone Number', 'ocmx'); ?><input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo $phone; ?>" /></label>
        	</p>
        	<p>
            <p><em> <?php _e('Linking your phone number adds compatibiity for mobile devices', 'ocmx'); ?></em></p>  
		        <label for="<?php echo $this->get_field_id('phone_link'); ?>">
		            <input type="checkbox" <?php if(isset($phone_link) && $phone_link == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('phone_link'); ?>" name="<?php echo $this->get_field_name('phone_link'); ?>">
		            <?php _e('Link Phone Number', 'ocmx'); ?>
		        </label>
		    </p>
        	<p>
        		<label for="<?php echo $this->get_field_id('email'); ?>"> <?php _e('Email Address', 'ocmx'); ?><input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" /></label>
        	</p>
        	<p>
		        <label for="<?php echo $this->get_field_id('email_link'); ?>">
		            <input type="checkbox" <?php if(isset($email_link) && $email_link == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('email_link'); ?>" name="<?php echo $this->get_field_name('email_link'); ?>">
		             <?php _e('Link Email Address', 'ocmx'); ?>
		        </label>
		    </p>
		    <p>
				<label for="<?php echo $this->get_field_id('page_choose'); ?>"><?php _e('Link to Contact Page', 'ocmx'); ?></label>
		    	<?php $pages = get_pages();?>
		          
		     	<select size="1" class="widefat" id="<?php echo $this->get_field_id("page_choose"); ?>" name="<?php echo $this->get_field_name("page_choose"); ?>">
		          
		          	<option <?php if(isset($filterval) && $filterval == 0){echo "selected=\"selected\"";} ?> value="0"><?php _e('Choose a Page', 'ocmx'); ?></option>
		          
		        	<?php foreach($pages as $page) :
		
		                $use_value =  $page->ID;
		                     
		                if($use_value == $page_choose)
		                    {$selected = " selected='selected' ";}
		                else
		                    {$selected = " ";} ?>
		                    
		        		<option <?php echo $selected; ?> value="<?php echo $use_value; ?>"><?php echo $page->post_title; ?></option>
		        		
		        	<?php endforeach; ?>
		        
		         </select>
                 <p><em>Use the <a href="edit.php?post_type=page">Pages</a> section to create a Contact page.</em></p>
			</p>
           
        <?php 
    }

} // class FooWidget

//This sample widget can then be registered in the widgets_init hook:

// register FooWidget widget
add_action('widgets_init', create_function('', 'return register_widget("contact_widget");'));

?>